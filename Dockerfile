FROM python:3.7

# Do not buffer output from python
ENV PYTHONUNBUFFERED 1

WORKDIR /code

COPY . /code/

RUN pip install -r requirements/prod.txt

CMD ["gunicorn", "-w", "3", "-b", "0.0.0.0:8000", "mannhulletweb2.wsgi"]

# tasks.py
from .models import Mannhullitt


def update_user_grades():
    users = Mannhullitt.objects.exclude(year=9)
    for user in users:
        if user.year < 5:
            user.year += 1
        elif user.year == 5 or user.year == 6:
            user.year = 9
        user.save()


def update_rank_users():
    # Update ranks for 4th-grade students
    Mannhullitt.objects.filter(year=4).update(rank='Kvartermester')  # Adjust rank name if needed

    # Update ranks for 5th and 6th-grade students
    for user in Mannhullitt.objects.filter(year__in=[5, 6]):
        if user.rank in ['Fenrik', 'Løytnant']:
            user.rank = 'Kapteinløytnant'  # Adjust rank name if needed
        else:
            user.rank = 'Løytnant'  # Adjust rank name if needed
        user.save()

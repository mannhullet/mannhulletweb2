from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from .forms import GroupAdminForm
from .models import Mannhullitt


def activate_user(modeladmin, request, queryset):
    for user in queryset:
        user.is_active = True
        user.save()


activate_user.short_description = 'Activate user'


def update_grade(modeladmin, request, queryset):
    for user in queryset:
        if user.year < 9:  # Ignore alumni
            if user.year == 5 or user.year == 6:  # Fifth and sixth graders are made alumni.
                user.year = 9
            else:
                user.year = user.year + 1  # Everyone else is just bumped up
        user.save()


update_grade.short_description = 'Update grade'


def update_ranks(modeladmin, request, queryset):
    queryset.filter(year=5).update(rank='Kvartermester')
    queryset.filter(year=9).update(rank='Løytnant')
    modeladmin.message_user(request, "Ranks updated successfully.")


update_ranks.short_description = 'Update Ranks for 5th Graders and Alumni'


class MannhullittAdmin(UserAdmin):
    model = Mannhullitt
    ordering = ('email',)
    list_display = ['first_name', 'last_name', 'username', 'email', 'year', 'allergies', 'is_active', 'date_joined']
    search_fields = ('email', 'first_name', 'last_name', 'username')
    readonly_fields = ('date_joined', 'last_login')
    actions = [activate_user, update_grade, update_ranks]
    exclude = ()
    fieldsets = ()
    filter_horizontal = ()
    list_filter = ('year',)  # Filter by year

    def get_ordering(self, request):
        return ['year', 'email']  # Default ordering by year and then email


admin.site.register(Mannhullitt, MannhullittAdmin)

admin.site.unregister(Group)


class GroupAdmin(admin.ModelAdmin):
    # Use our custom form.
    form = GroupAdminForm
    # Filter permissions horizontal as well.
    filter_horizontal = ['permissions']


# Register the new Group ModelAdmin.
admin.site.register(Group, GroupAdmin)

# context_processor for availability for everyone
def is_styret_member(request):
    if not request.user.is_authenticated:
        return {'is_styret_member': False}
    return {'is_styret_member': request.user.groups.filter(name='Styret').exists()}


def is_undergruppeadmin(request):
    if not request.user.is_authenticated:
        return {'is_undergruppeadmin': False}
    return {'is_undergruppeadmin': request.user.groups.filter(name='Undergruppeadmin').exists()}

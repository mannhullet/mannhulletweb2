# Generated by Django 2.2.15 on 2020-09-08 08:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0019_mannhullitt_is_allergic'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mannhullitt',
            name='is_allergic',
            field=models.CharField(default=None, max_length=30),
        ),
    ]

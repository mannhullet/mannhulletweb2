# Generated by Django 2.2.28 on 2023-09-21 15:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0032_auto_20230921_1610'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mannhullitt',
            name='main_specialization',
            field=models.TextField(choices=[('Ikke valgt', 'Ikke valgt'), ('Marin kybernetikk', 'Marin kybernetikk'), ('Marin prosjektering og logistikk', 'Marin prosjektering og logistikk'), ('Marin hydrodynamikk', 'Marin hydrodynamikk'), ('Marin konstruksjonsteknikk', 'Marin konstruksjonsteknikk'), ('Marint maskineri', 'Marint maskineri'), ('Sikkerhet- og driftsledelse', 'Sikkerhet- og driftsledelse')], default='Ikke valgt'),
        ),
    ]

# Generated by Django 2.2.9 on 2020-03-18 11:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_remove_mannhullitt_graduation_year'),
    ]

    operations = [
        migrations.AddField(
            model_name='mannhullitt',
            name='username',
            field=models.CharField(default='mathins', max_length=30, unique=True),
            preserve_default=False,
        ),
    ]

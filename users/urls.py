from django.urls import path
from . import views
from django.views.generic.base import TemplateView
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, \
     PasswordResetConfirmView, PasswordResetCompleteView, PasswordChangeView, PasswordChangeDoneView
from .views import user_list_view, rank_update_view, create_fictive_user


urlpatterns = [
    path('opprett/', views.SignUp.as_view(), name='signup'),
    path('rediger/', views.editProfile, name='edit_profile'),
    path('suksess/', TemplateView.as_view(template_name='users/successful_registration.html'), name='success'),
    path('info/', views.UserView.as_view(), name='brukerinfo'),
    path('login/', views.CustomLoginView.as_view(template_name="users/login.html"), name='login'),
    path('password_reset/', PasswordResetView.as_view(template_name="users/password_reset_form.html"),
         name='password_reset'),
    path('password_reset/done/', PasswordResetDoneView.as_view(template_name="users/password_reset_done.html"),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>/', PasswordResetConfirmView.as_view(template_name="users/password_reset_confirm.html"),
         name='password_reset_confirm'),
    path('reset/done/', PasswordResetCompleteView.as_view(template_name="users/password_reset_complete.html")),
    path('password_change/', PasswordChangeView.as_view(template_name="users/password_change_form.html")),
    path('password_change/done/', PasswordChangeDoneView.as_view(template_name="users/password_change_done.html")),
    path('users/', user_list_view, name='user_list'),
    path('users/update-rank/<int:user_id>/', rank_update_view, name='rank_update'),
    path('create-fictive-user/', create_fictive_user, name='create_fictive_user'),
    path('update-grades/', views.update_grades, name='update-grades'),
    path('update-rank/', views.update_rank, name='update-rank'),
]

from django.db import models
from django.contrib.auth.models import (AbstractBaseUser,
                                        BaseUserManager, PermissionsMixin)
from datetime import datetime


class MannhullitManager(BaseUserManager):
    def create_user(self, email, username, first_name, last_name,
                    allergies, enrollment_year, year, main_specialization, rank, password=None):
        if not email:
            raise ValueError("Users must have an email adress")
        user = self.model(email=self.normalize_email(email),
                          username=username,
                          first_name=first_name,
                          last_name=last_name,
                          allergies=allergies,
                          enrollment_year=enrollment_year,
                          year=year,
                          main_specialization=main_specialization,
                          rank=rank)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_fictive_user(self, first_name, last_name, rank, username=None, email=None):
        # Set a default enrollment year
        default_enrollment_year = 2020  # or any other suitable default value

        if not username:
            username = f"{first_name}_{last_name}".lower()

        if not email:
            email = username.replace(" ", "_") + "@fiktivbruker.no"

        user = self.model(
            username=username,
            first_name=first_name,
            last_name=last_name,
            rank=rank,
            year=9,
            enrollment_year=default_enrollment_year,  # Set the default value here
            email=email,
            is_fictive=True
        )

        user.set_unusable_password()
        user.save(using=self._db)
        return user

    def create_superuser(
            self, email, username, first_name, last_name,
            allergies, enrollment_year, year, password, main_specialization=None, rank=None):
        if not email:
            raise ValueError("Users must have an email adress")
        user = self.create_user(email=self.normalize_email(email),
                                username=username,
                                first_name=first_name,
                                last_name=last_name,
                                allergies=allergies,
                                enrollment_year=enrollment_year,
                                year=year,
                                main_specialization=main_specialization,
                                rank=rank,
                                password=password, )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save(using=self._db)
        return user


class Mannhullitt(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(verbose_name='email', max_length=60, unique=True)
    username = models.CharField(max_length=30, unique=True)
    date_joined = models.DateTimeField(
        verbose_name='date joined',
        auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    allergies = models.TextField(max_length=50, blank=True)
    enrollment_year = models.IntegerField(blank=False)
    accepted_terms = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    dots = models.IntegerField(default=0)
    time_last_changed = models.DateField(null=True, blank=True)
    card_id = models.BigIntegerField(blank=True, null=True)
    # ID number read from RFID-chip. For example the student card
    is_fictive = models.BooleanField(default=False)
    SPECIALIZATION_CHOICES = (
        ('Ikke valgt', 'Ikke valgt'),
        ('Marin kybernetikk', 'Marin kybernetikk'),
        ('Marin prosjektering og logistikk', 'Marin prosjektering og logistikk'),
        ('Marin hydrodynamikk', 'Marin hydrodynamikk'),
        ('Marin konstruksjonsteknikk', 'Marin konstruksjonsteknikk'),
        ('Marint maskineri', 'Marint maskineri'),
        ('Sikkerhet- og driftsledelse', 'Sikkerhet- og driftsledelse')
    )
    main_specialization = models.TextField(choices=SPECIALIZATION_CHOICES, default='Ikke valgt',
                                           null=False, blank=False)

    RANK_CHOICES = (
        ('Admiral', 'Admiral'),
        ('Æresadmiral', 'Æresadmiral'),
        ('Viseadmiral', 'Viseadmiral'),
        ('Kontreadmiral', 'Kontreadmiral'),
        ('Flagg-Kommandør', 'Flagg-Kommandør'),
        ('Kommandør', 'Kommandør'),
        ('Kommandørkaptein', 'Kommandørkaptein'),
        ('Orlogskaptein', 'Orlogskaptein'),
        ('Kapteinløytnant', 'Kapteinløytnant'),
        ('Løytnant', 'Løytnant'),
        ('Fenrik', 'Fenrik'),
        ('Kvartermester', 'Kvartermester'),
        ('Kadett', 'Kadett'),
        ('Phoster', 'Phoster')
    )
    rank = models.CharField(max_length=50, choices=RANK_CHOICES, default='Kadett', blank=False)

    YEAR_CHOICES = (
        (1, '1. klasse'),
        (2, '2. klasse'),
        (3, '3. klasse'),
        (4, '4. klasse'),
        (5, '5. klasse'),
        (6, '6. klasse'),
        (9, 'Alumni'),
    )

    year = models.IntegerField(choices=YEAR_CHOICES, default=1)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = [
        'email', 'first_name', 'last_name',
        'allergies', 'enrollment_year', 'year', 'main_specialization', 'rank'
    ]

    objects = MannhullitManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.first_name + ' ' + self.last_name

    def register_dot(self):
        if (self.dots + 1) > 3:
            return
        else:
            self.dots += 1
        self.save()

    def has_logged_in_this_semester(self):
        if not self.last_login:
            return False

        today = datetime.today().date()
        year = today.year

        # Define semester start and end dates
        spring_start = datetime(year, 1, 1).date()
        spring_end = datetime(year, 6, 10).date()
        autumn_start = datetime(year, 8, 1).date()
        autumn_end = datetime(year, 12, 31).date()

        # Check if last login falls within the current semester
        if spring_start <= today <= spring_end:
            return spring_start <= self.last_login.date() <= spring_end
        else:
            return autumn_start <= self.last_login.date() <= autumn_end

    def needs_specialization_update(self):
        current_month = datetime.now().month
        is_spring_semester = 1 <= current_month <= 6

        # Logic for determining if specialization update is required
        if self.year >= 4:
            return not self.main_specialization or self.main_specialization == 'Ikke valgt'
        elif self.year == 3 and is_spring_semester:
            return not self.main_specialization or self.main_specialization == 'Ikke valgt'
        return False

from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from .models import Mannhullitt
from django.contrib.auth import get_user_model, authenticate
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _
from django.forms import ModelForm
from datetime import datetime


class CustomAuthForm(AuthenticationForm):
    error_messages = {
        'invalid_login': _(
            "Vennligst oppgi korrekt brukernavn og passord. Merk at det kan være forskjell på små og store bokstaver."
        ),
        'inactive': _("Brukeren er ikke godkjent enda. Om dette vedvarer, ta kontakt med web@mannhullet.no"),
    }

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                try:
                    user_temp = User.objects.get(username=username)
                except Mannhullitt.DoesNotExist:
                    user_temp = None
                if user_temp is not None and user_temp.check_password(password):
                    self.confirm_login_allowed(user_temp)
                else:
                    raise forms.ValidationError(
                        self.error_messages['invalid_login'],
                        code='invalid_login',
                        params={'username': self.username_field.verbose_name},
                    )

        return self.cleaned_data


class MannhullittCreationForm(UserCreationForm):
    password1 = forms.CharField(
        label=("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )

    password2 = forms.CharField(
        label=("PasswordConf"),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )

    accepted_terms = forms.BooleanField(required=True)

    class Meta(UserCreationForm):
        model = Mannhullitt
        fields = [
            'first_name',
            'last_name',
            'email',
            'year',
            'allergies',
            'accepted_terms',
            'enrollment_year',
            'username',
            'card_id',
        ]

        labels = {
            'first_name': 'Fornavn',
            'last_name': 'Etternavn',
            'username': 'Brukernavn',
            'email': 'Epost',
            'year': 'Trinn',
            'allergies': 'Allergier',
            'enrollment_year': 'Startår',
            'accepted_terms': 'Godkjenn',
            'card_id': 'Kortnummer',
        }

        help_texts = {
            'email':
                "Her kan du enten skrive inn studmailen eller en privat mail",
            'card_id':
                'IKKE PÅKREVD. 7-13 sifret tallrekke som du finner' +
                'på baksiden av ditt studentkort. ' +
                'Det er tallrekken som står etter EM som skal skrives inn. ' +
                'Om tallrekken starter med 0 skal ikke denne tas med.',
        }

        widgets = {
            'first_name': forms.TextInput(
                attrs={'class': 'form-control', 'Placeholder': 'Fornavn'}),
            # Placeholder for signup-fields
            'username': forms.TextInput(
                attrs={'class': 'form-control', 'Placeholder': 'Brukernavn'}),
            'last_name': forms.TextInput(
                attrs={'class': 'form-control', 'Placeholder': 'Etternavn'}),
            'email': forms.TextInput(
                attrs={'class': 'form-control', 'Placeholder': 'Email'}),
            'allergies': forms.TextInput(
                attrs={'class': 'form-control'}),
            'enrollment_year': forms.NumberInput(
                attrs={'class': 'form-control'}),
            'password1': forms.TextInput(
                attrs={'class': 'form-control'}),
            'card_id': forms.TextInput(
                attrs={'class': 'form-control'}),
        }


class MannhullittChangeForm(UserChangeForm):
    class Meta(UserChangeForm):
        model = Mannhullitt
        fields = [
            'email',
            'first_name',
            'last_name',
            'main_specialization',
            'allergies',
            'year',
            'card_id'
        ]
        labels = {
            'first_name': ('Fornavn'),
            'last_name': ('Etternavn'),
            'main_specialization': ('Fordypning'),
            'email': ('Epost'),
            'allergies': ('Allergier'),
            'year': ('Klassetrinn'),
            'card_id': ('Kortnummer')
        }

        help_texts = {
            'card_id':
                '7-13 sifret tallrekke som du finner på baksiden' +
                'av ditt studentkort.\n' +
                'Det er tallrekken som står etter EM som skal skrives inn.' +
                ' Om tallrekken starter med 0 skal ikke denne tas med.',
        }

        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'allergies': forms.TextInput(attrs={'class': 'form-control'}),
            'year': forms.Select(attrs={'class': 'form-control'}),
            'card_id': forms.TextInput(attrs={'class': 'form-control'})
        }

    def clean(self):
        cleaned_data = super().clean()
        main_specialization = cleaned_data.get('main_specialization')
        year = cleaned_data.get('year')

        current_month = datetime.now().month

        # Determine if it's the second semester
        is_second_semester = 1 <= current_month <= 6

        # Determine if it's the autumn semester
        is_autumn_semester = 7 <= current_month <= 12

        # Logic for requiring main_specialization
        if year is not None:
            # Require specialization for 4th, 5th, or 6th grade in any semester
            if year >= 4:
                if not main_specialization or main_specialization == 'Ikke valgt':
                    self.add_error('main_specialization', 'Du må velge en hovedspesialisering.')

            # If it's the autumn semester, 3rd grade does not require specialization
            elif year == 3 and is_autumn_semester:
                # Do not require specialization for 3rd grade in the autumn
                pass

            # If it's the second semester (spring), 3rd grade requires specialization
            elif year == 3 and is_second_semester:
                if not main_specialization or main_specialization == 'Ikke valgt':
                    self.add_error('main_specialization', 'Du må velge en hovedspesialisering.')

        cd = self.cleaned_data
        if cd.get('first_name') == cd.get('email'):
            self.add_error(
                'first_name', "Du kan ikke hete det samme som eposten din.")
        return cd

    def clean_year(self):
        year = self.cleaned_data.get('year')
        if year is not None and year != self.initial['year']:
            if self.instance.time_last_changed:
                # Corrected usage here
                time_since_last_change = datetime.today().date() - self.instance.time_last_changed
                if time_since_last_change.days < 365:
                    raise forms.ValidationError(
                        'Du kan kun endre klassetrinn engang i året. Mener du dette er feil, kontakt styret.'
                    )
        elif year is None:
            raise forms.ValidationError('Du må velge et klassetrinn.')  # Assuming year is a required field
        return year


User = get_user_model()


class GroupAdminForm(forms.ModelForm):
    class Meta:
        model = Group
        exclude = []

    mannhullitt = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        required=False,
        widget=FilteredSelectMultiple('mannhullitt', False)
    )

    def __init__(self, *args, **kwargs):
        # Do the normal form initialisation
        super(GroupAdminForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            self.fields['mannhullitt'].initial = self.instance.user_set.all()

    def save_m2m(self):
        self.instance.user_set.set(self.cleaned_data['mannhullitt'])

    def save(self, *args, **kwargs):
        instance = super(GroupAdminForm, self).save()
        self.save_m2m()
        return instance


class RankUpdateForm(ModelForm):
    class Meta:
        model = Mannhullitt
        fields = ['rank']


class FictiveUserCreationForm(forms.ModelForm):
    class Meta:
        model = Mannhullitt
        fields = ['first_name', 'last_name', 'rank']

    def save(self, commit=True):
        user = Mannhullitt.objects.create_fictive_user(
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name'],
            rank=self.cleaned_data['rank'],
        )
        return user

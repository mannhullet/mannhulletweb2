from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.contrib.auth.views import LoginView
from .forms import MannhullittCreationForm, MannhullittChangeForm, CustomAuthForm
from django.contrib.auth.forms import PasswordChangeForm
from .models import Mannhullitt
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from .forms import RankUpdateForm, FictiveUserCreationForm
from django.db.models import Q
from .tasks import update_user_grades, update_rank_users
from django.conf import settings
from datetime import datetime
from django.core.exceptions import PermissionDenied


class SignUp(generic.CreateView):
    form_class = MannhullittCreationForm
    template_name = 'users/signup.html'
    success_url = reverse_lazy('success')


class UserView(generic.base.TemplateView):
    template_name = 'users/user_info.html'


class CustomLoginView(LoginView):
    authentication_form = CustomAuthForm


# ---------------------------------------------------------------------------------------------
# This view is currently not used, the Signup class-based view will be used until
# i figure out the enviroment varbiables
#
# def signup(request):
#    form_class = MannhullittCreationForm
#    template_name = 'users/signup.html'
#    form = form_class(request.POST or None)
#    context = {'public': settings.GOOGLE_RECAPTCHA_PUBLIC_KEY,
#               'form': form}
#    if request.method == 'POST':
#        if form.is_valid():
#            # reCAPTCHA validation begins
#            recaptcha_response = request.POST.get('g-recaptcha-response')
#            data = {
#                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
#                'response': recaptcha_response
#            }
#            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
#            results = r.json()
#
#            if results['success']:
#                form.save()
#                return redirect('success')
#            else:
#                messages.error(request, 'Invalid reCAPTCHA. Please try again.')
#    return render(request, template_name, context)
# ---------------------------------------------------------------------------------------------


def editProfile(request):
    user = request.user
    password_form = PasswordChangeForm(request.user)
    change_form = MannhullittChangeForm(instance=request.user)

    if request.method == 'POST':
        if "edit_user" in request.POST:
            change_form = MannhullittChangeForm(request.POST, instance=request.user)
            if change_form.is_valid():
                instance = change_form.save(commit=False)
                # Check if the year has been changed and the last change was more than a year ago
                if 'year' in change_form.changed_data:
                    if instance.time_last_changed is not None:
                        time_since_last_change = datetime.today().date() - instance.time_last_changed
                        if time_since_last_change.days < 365:
                            messages.error(request, 'You can only change your class year once per year.')
                            return render(request, 'users/edit_profile.html', {
                                'change_form': change_form,
                                'password_form': password_form,
                                'user': user
                            })
                    instance.time_last_changed = datetime.today().date()
                instance.save()
                change_form.save_m2m()
                messages.success(request, 'Profilen din har blitt oppdatert!')
                return redirect('brukerinfo')
        elif "change_password" in request.POST:
            password_form = PasswordChangeForm(request.user, request.POST)
            if password_form.is_valid():
                user = password_form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, 'Passordet ditt har blitt oppdatert!')
                return redirect('brukerinfo')  # Adjust the redirect as necessary
            else:
                messages.error(request, 'Korriger feilen under.')

    return render(request, 'users/edit_profile.html', {
        'change_form': change_form,
        'password_form': password_form,
        'user': user
    })


def is_styret_member(user):
    if user.groups.filter(name='Styret').exists():
        return True
    else:
        raise PermissionDenied


@login_required
@user_passes_test(is_styret_member)
def create_fictive_user(request):
    if request.method == 'POST':
        form = FictiveUserCreationForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')

            # Check if a user with this combined first and last name already exists
            if Mannhullitt.objects.filter(first_name=first_name, last_name=last_name).exists():
                error_message = 'En bruker med dette navnet eksisterer allerede.'
                form.add_error('last_name', error_message)
                # No need to add a non-field error here.
            else:
                # Save the user
                form.save()
                return redirect('user_list')  # Redirect to the user list page
        # If form is not valid or if there are errors, fall through to re-render the form
    else:
        form = FictiveUserCreationForm()

    return render(request, 'users/fictive_user_creation_template.html', {'form': form})


@login_required
@user_passes_test(is_styret_member)
def user_list_view(request):
    users_to_be_loaded = 50
    users_list = Mannhullitt.objects.all().order_by('first_name')

    search_query = request.GET.get('search', '')
    if search_query:
        users_list = users_list.filter(
            Q(first_name__icontains=search_query) |
            Q(last_name__icontains=search_query)
        )

    if request.is_ajax():
        class_year = request.GET.get('class_year')

        if class_year:
            users_list = users_list.filter(year=class_year)

        paginator = Paginator(users_list, users_to_be_loaded)
        page_number = request.GET.get('page', 1)
        users_page = paginator.get_page(page_number)

        if users_page.has_next():
            next_page_number = users_page.next_page_number()
        else:
            next_page_number = None

        users_html = render_to_string('users/users_list_partial.html', {'users': users_page}, request=request)
        output_data = {
            'users_html': users_html,
            'has_next': users_page.has_next(),
            'next_page_number': next_page_number
        }
        return JsonResponse(output_data)

    # Initial page load, not AJAX
    paginator = Paginator(users_list, users_to_be_loaded)
    page_number = request.GET.get('page', 1)
    users_page = paginator.get_page(page_number)
    class_years = Mannhullitt.YEAR_CHOICES

    return render(request, 'users/user_list.html', {'users': users_page, 'class_years': class_years})


@login_required
@user_passes_test(is_styret_member)
def rank_update_view(request, user_id):
    user = Mannhullitt.objects.get(id=user_id)
    if request.method == 'POST':
        form = RankUpdateForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect('user_list')  # Redirect back to the user list
    else:
        form = RankUpdateForm(instance=user)

    return render(request, 'users/rank_update.html', {'form': form})


def update_grades(request):
    # Check for secret token in the request header
    secret_token = request.headers.get('Authorization')
    expected_token = settings.DATABASE_UPDATE_SECRET_KEY

    if secret_token == f"Bearer {expected_token}":
        update_user_grades()
        return HttpResponse("Grades update initiated", status=202)
    else:
        raise PermissionDenied


def update_rank(request):
    # Check for secret token in the request header
    secret_token = request.headers.get('Authorization')
    expected_token = settings.DATABASE_UPDATE_SECRET_KEY

    if secret_token == f"Bearer {expected_token}":
        update_rank_users()
        return HttpResponse("Grades update initiated", status=202)
    else:
        raise PermissionDenied


def home_view(request):
    if request.user.is_authenticated and request.user.needs_specialization_update():
        messages.info(request, 'Du må velge fordypning.')  # Add a message
        return redirect('edit_profile')

    return render(request, 'home.html')


@login_required
def marin_hjelp(request):
    password = request.GET.get('password', '')
    if password == 'neptunus1917':  # Replace with your actual password
        # Redirect to the actual secret URL
        return HttpResponseRedirect(
            'https://drive.google.com/drive/folders/1OVcBFlyh9-97y1j0zrjXsJI10wsYUbUc?usp=sharing')
    else:
        raise PermissionDenied

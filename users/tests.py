from users.models import Mannhullitt
from django.contrib.auth.models import Group
from django.test import TestCase
from django.contrib.auth import authenticate
from django.utils import timezone
from django.urls import reverse


class BasicUserTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'email': 'example@stud.ntnu.no',
            'first_name': 'Test',
            'last_name': 'Testesen',
            'allergies': '',
            'enrollment_year': 2018,
            'year': 3,
            'main_specialization': 'Marin kybernetikk',
            'is_active': True}
        self.user = Mannhullitt.objects.create(**self.credentials)
        self.user.set_password('secret')
        self.user.save()

    def test_user(self):
        self.assertTrue(self.user.email, 'example@stud.ntnu.no')

    def test_login(self):
        user = authenticate(username='testuser', password='secret')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_wrong_password(self):
        user = authenticate(username='testuser', password='sekret')
        self.assertFalse((user is not None) and user.is_authenticated)

    def test_wrong_username(self):
        user = authenticate(username='user', password='secret')
        self.assertFalse((user is not None) and user.is_authenticated)

    def test_login_view(self):
        response = self.client.post('/brukere/login/', {'username': 'testuser', 'password': 'secret'})
        self.assertRedirects(response, '/')

    def test_wrong_username_view(self):
        response = self.client.post('/brukere/login/', {'username': 'user', 'password': 'secret'})
        self.assertEqual(response.status_code, 200)

    def test_wrong_pssword_view(self):
        response = self.client.post('/brukere/login/', {'username': 'testuser', 'password': 'wrong'})
        self.assertEqual(response.status_code, 200)

    def test_returns_email(self):
        self.assertEqual(self.user.email, str(self.user))

    def test_get_full_name(self):
        self.assertEqual('Test Testesen', self.user.get_full_name())

    def test_register_valid_dot(self):
        self.user.register_dot()
        self.assertEqual(1, self.user.dots)

    def test_register_dots_full(self):
        self.user.dots = 3
        self.user.register_dot()
        self.assertEqual(3, self.user.dots)

    def test_inactive_user_denied(self):
        self.user.is_active = False
        self.user.save()
        user = authenticate(username='user', password='secret')
        self.assertFalse((user is not None) and user.is_authenticated)


class EventGroupPermissions(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'email': 'example@stud.ntnu.no',
            'first_name': 'Test',
            'last_name': 'Testesen',
            'allergies': '',
            'enrollment_year': 2018,
            'year': 3,
            'is_active': True}
        self.user = Mannhullitt.objects.create(**self.credentials)
        self.user.set_password('secret')
        self.user.save()
        self.event_data = {
            'title': 'Test2',
            'description': 'Testing',
            'start_time': timezone.now() + timezone.timedelta(hours=2),
            'end_time': timezone.now() + timezone.timedelta(hours=3),
            'registration': True,
            'registration_starts': timezone.now(),
            'registration_ends': timezone.now() + timezone.timedelta(hours=1),
            'max_attendees': 10,
            'location': 'Tyholt',
            'organizer': 'Webgruppa',
            'invited_years': [1, 2, 3, 4, 5, 6, 9],
            'business_presentation': False,
            'company': '',
        }

    def test_can_add_styret(self):
        self.group = Group(name="Styret")
        self.group.save()
        self.user.groups.add(self.group)
        self.user.save()
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:create_event'), data=self.event_data)
        self.assertEqual(response.status_code, 200)

    def test_can_add_undergruppeadmin(self):
        self.group = Group(name="Undergruppeadmin")
        self.group.save()
        self.user.groups.add(self.group)
        self.user.save()
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:create_event'), data=self.event_data)
        self.assertEqual(response.status_code, 200)

    def test_can_add_bedriftskontakt(self):
        self.group = Group(name="Bedriftskontakt")
        self.group.save()
        self.user.groups.add(self.group)
        self.user.save()
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:create_event'), data=self.event_data)
        self.assertEqual(response.status_code, 200)

    def test_can_add_webgruppen(self):
        self.group = Group(name="Webgruppen")
        self.group.save()
        self.user.groups.add(self.group)
        self.user.save()
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:create_event'), data=self.event_data)
        self.assertEqual(response.status_code, 200)

from django import forms
from .models import JobAd, Member, Location, Company, Semester, CompanyLog
from django.forms.widgets import CheckboxSelectMultiple
"""

---These will be added when internal BK pages are being developed---
class CompanyChangeForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = [
            'company_name',
            'company_summary',
            'company_logo'
            'company_homepage',
            'company_contact',
            'company_contact_email',
            'company_contact_phone',
        ]
        labels = {
            'company_name': ('Firmanavn'),
            'company_summary': ('Beskrivelse'),
            'company_logo': ('Logo'),
            'company_homepage': ('Hjemmeside'),
            'company_contact': ('Kontaktperson'),
            'company_contact_email': ('Epost'),
            'company_contact_phone': ('Telefon'),
        }
"""


class CompanyLogAddForm(forms.ModelForm):
    def __init__(self, **kwargs):
        self.log_user = kwargs.pop('log_user', None)
        self.log_timeStamp = kwargs.pop('log_timeStamp', None)
        self.company = kwargs.pop('company', None)
        self.semester = kwargs.pop('semester', None)
        super(CompanyLogAddForm, self).__init__(**kwargs)

    def save(self, commit=True):
        obj = super(CompanyLogAddForm, self).save(commit=False)
        obj.log_user = self.log_user
        obj.log_timeStamp = self.log_timeStamp
        obj.company = self.company
        obj.semester = self.semester
        if commit:
            obj.save()
        return obj

    class Meta:
        model = CompanyLog
        fields = [
            'log_status',
            'log_contact_person',
            'log_contact_info',
            'log_comment',
        ]
        labels = {
            'log_status': ('Status'),
            'log_contact_person:': ('Kontaktperson'),
            'log_contact_info': ('Kontaktinfo (mail eller telefon)'),
            'log_comment': ('Kommentar'),
        }


class CompanyCreationForm(forms.ModelForm):

    class Meta:
        model = Company
        fields = [
            'company_name',
            'company_summary',
            'company_logo',
            'company_homepage',
            'company_contact',
            'company_contact_email',
            'company_contact_phone',
        ]
        labels = {
            'company_name': ('Firmanavn'),
            'company_summary': ('Beskrivelse'),
            'company_logo': ('Logo'),
            'company_homepage': ('Hjemmeside'),
            'company_contact': ('Kontaktperson'),
            'company_contact_email': ('Epost'),
            'company_contact_phone': ('Telefon'),
        }


class JobAdForm(forms.ModelForm):

    location = forms.ModelMultipleChoiceField(queryset=Location.objects.all().order_by(
        'title'), widget=CheckboxSelectMultiple,)

    class Meta:
        model = JobAd
        fields = [
            'company',
            'title',
            'location',
            'deadline',
            'position',
            'description',
            'contact_person',
            'contact_phone',
            'contact_mail',
            'homepage',
            'priority_BK',
            'priority_Mannhullet',
        ]
        labels = {
            'company': ('Firma'),
            'title': ('Tittel'),
            'location': ('Lokasjon'),
            'deadline': ('Frist'),
            'position': ('Stilling'),
            'description': ('Beskrivelse'),
            'contact_person': ('Kontaktperson'),
            'contact_phone': ('Kontaktperson telefon'),
            'contact_mail': ('Mail'),
            'homepage': ('Søknadsside'),
            'priority_BK': ('Prioritert BK'),
            'priority_Mannhullet': ('Prioritert Mannhullet'),
        }


class MemberAddForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = [
            'member_name',
            'member_email',
            'member_image',
            'member_position',
            'member_priority',
        ]
        labels = {
            'member_name': ('Navn'),
            'member_email': ('Email'),
            'member_image': ('Bilde'),
            'member_position': ('Stilling'),
            'member_priority': ('Prioriteringsnummer (Leder=1, nestleder=2, bedriftskontakt=100)'),
        }


class LocationAddForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = [
            'title',
        ]
        labels = {
            'title': ('Ny lokasjon:')
        }


class NewSemesterForm(forms.ModelForm):
    class Meta:
        model = Semester
        fields = [
            'semester'
        ]
        labels = {
            'semester': ('Semester'),
        }


class NewDeadlineForm(forms.Form):
    deadline = forms.DateField(label='Frist')

from .models import JobAd, Location, CallList
import django_filters


class JobAdFilter(django_filters.FilterSet):

    location = django_filters.ModelChoiceFilter(queryset=Location.objects.all().order_by('title'))

    class Meta:
        model = JobAd
        fields = [
            'position',
            'location',
        ]

    def __init__(self, *args, **kwargs):
        super(JobAdFilter, self).__init__(*args, **kwargs)
        self.filters['position'].label = "Stillingstyper"
        self.filters['location'].label = "Lokasjon"


class CallListFilter(django_filters.FilterSet):

    class Meta:
        model = CallList
        fields = [
            'company_asignee',
        ]

    def __init__(self, *args, **kwargs):
        super(CallListFilter, self).__init__(*args, **kwargs)
        self.filters['company_asignee'].label = "Ringelisten til"

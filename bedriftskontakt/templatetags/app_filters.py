from django import template

register = template.Library()


@register.filter
def index(sequence, position):
    return sequence[position]


@register.filter
def has_BK_admin_permission(user):
    permitted_groups = ['BedriftskontaktAdmin']
    if user.is_superuser:
        return True
    else:
        for group in permitted_groups:
            if user.groups.filter(name=group).exists():
                return True
        return False


@register.filter
def has_BK_permission(user):
    permitted_groups = ['Bedriftskontakt', 'BedriftskontaktAdmin']
    if user.is_superuser:
        return True
    else:
        for group in permitted_groups:
            if user.groups.filter(name=group).exists():
                return True
        return False

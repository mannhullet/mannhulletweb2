from django.db import models
from django.utils import timezone
from tinymce.models import HTMLField
from users.models import Mannhullitt
from django.template.defaultfilters import slugify
# Create your models here.


class Member(models.Model):
    member_name = models.TextField(max_length=50)
    member_email = models.EmailField(max_length=50)
    member_image = models.ImageField(upload_to='BKmembers', null=True, blank=True)
    member_position = models.TextField(max_length=30)
    member_priority = models.IntegerField(default=100)

    def __str__(self):
        return self.member_name


class Company(models.Model):

    company_name = models.TextField(max_length=60, unique=True)
    company_summary = HTMLField(max_length=2000)
    company_homepage = models.TextField(max_length=100, null=True, blank=True)
    company_logo = models.ImageField(upload_to='Companies', null=False, blank=False, default="static/img/BK.png")
    # Company contact person details
    company_contact = models.TextField(max_length=100, null=True, blank=True)
    company_contact_email = models.EmailField(max_length=300, null=True, blank=True)
    company_contact_phone = models.TextField(max_length=30, null=True, blank=True)

    class Meta:
        ordering = ['company_name']

    def __str__(self):
        return self.company_name


class Semester(models.Model):
    semester = models.CharField(max_length=20)
    slug = models.SlugField(max_length=20, unique=True)

    def __str__(self):
        return self.semester

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.semester)
        super(Semester, self).save(*args, **kwargs)


class CompanyLog(models.Model):
    Status_interested = 'Interessert'
    Status_not_interested = 'Ikke interessert'
    Status_not_contacted = 'Ikke kontaktet'
    Status_not_relevant = 'Ikke relevant'
    Status_no_answer = 'Ikke fått svar'
    Status_coming = 'Kommer'
    Status_sponsor = 'Sponsor'
    LOG_STATUS_CHOICES = [
        (Status_interested, 'Interessert'),
        (Status_not_interested, 'Ikke interesert'),
        (Status_not_contacted, 'Ikke kontaktet'),
        (Status_not_relevant, 'Ikke relevant'),
        (Status_no_answer, 'Ikke fått svar'),
        (Status_coming, 'Kommer'),
        (Status_sponsor, 'Sponsor'),
    ]
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    log_timeStamp = models.DateTimeField(default=timezone.now)
    log_comment = models.TextField(max_length=300)
    log_contact_person = models.CharField(max_length=100)
    log_contact_info = models.CharField(max_length=100)
    semester = models.ForeignKey(Semester, on_delete=models.SET_NULL, null=True, blank=True)
    log_status = models.TextField(choices=LOG_STATUS_CHOICES, default=Status_not_contacted)
    log_user = models.CharField(max_length=100)

    def __str__(self):
        return self.company.company_name


class Location(models.Model):
    title = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.title


class CallListManager(models.Manager):
    def create_CallList(self, company, semester):
        calllist = self.create(company=company, semester=semester, company_asignee=None, deadline=None)
        return calllist

    def change_deadline(self, deadline):
        calllist = self.update(deadline=deadline)
        return calllist


class CallList(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    company_asignee = models.ForeignKey(
        Mannhullitt,
        on_delete=models.SET_NULL,
        null=True, blank=True,
        limit_choices_to={'groups__name': "Bedriftskontakt"})
    semester = models.ForeignKey(Semester, on_delete=models.SET_NULL, null=True, blank=True)
    deadline = models.DateField(null=True, blank=True)
    objects = CallListManager()

    class Meta:
        ordering = ['company']

    def __str__(self):
        return self.company.company_name


class JobAd(models.Model):
    Summer_job = 'Sommerjobb'
    Fulltime_job = 'Fulltidsjobb'
    Trainee = 'Trainee'
    Parttime_job = 'Deltidsjobb'
    Else = 'Annet'
    AD_POSITION_CHOICES = [
        (Summer_job, 'Sommerjobb'),
        (Fulltime_job, 'Fulltidsjobb'),
        (Trainee, 'Trainee'),
        (Parttime_job, 'Deltidsjobb'),
        (Else, 'Annet'),

    ]
    location = models.ManyToManyField(Location)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    deadline = models.DateField(null=True, blank=True)
    title = models.CharField(max_length=100)
    description = HTMLField()
    position = models.CharField(max_length=50, choices=AD_POSITION_CHOICES, default=Fulltime_job)
    contact_person = models.CharField(max_length=50, null=True, blank=True)
    contact_phone = models.CharField(max_length=50, null=True, blank=True)
    contact_mail = models.EmailField(max_length=100, null=True, blank=True)
    homepage = models.URLField(max_length=100)
    priority_BK = models.BooleanField(default=False)
    priority_Mannhullet = models.BooleanField(default=False)

    def __str__(self):
        return self.company.company_name + " - " + self.title

from django.contrib import admin
from .models import Member, JobAd, Company, Location, CompanyLog, CallList, Semester


class BedriftskontaktAdmin(admin.ModelAdmin):
    list_display = [
        'members',
        'ads',
        'companies',
        'locations',
        'log',
        'call_list',
        'semester',
    ]


admin.site.register(Member)
admin.site.register(JobAd)
admin.site.register(Company)
admin.site.register(Location)
admin.site.register(CompanyLog)
admin.site.register(CallList)
admin.site.register(Semester)

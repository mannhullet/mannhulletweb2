from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404
from bedriftskontakt.forms import JobAdForm, MemberAddForm, LocationAddForm,\
    CompanyCreationForm, NewSemesterForm, NewDeadlineForm, CompanyLogAddForm
from .models import Member, JobAd, Company, CompanyLog, CallList, Semester
from events.models import Event
from django.core.exceptions import PermissionDenied
from django.utils import timezone
from django.urls import reverse
from bedriftskontakt.filters import JobAdFilter, CallListFilter
from datetime import datetime
from django.forms.models import inlineformset_factory
from django.db.models import Q, F
""""
-------This is used for internal BK functions and will be added later when public site is finished------


def change_company(request, pk):
    company = get_object_or_404(Company, pk=pk)
    form = CompanyChangeForm(instance=company)
    if request.method == 'POST':
        form = CompanyChangeForm(data=request.POST, instance=company)
        form.save()
        company.save()
        return HttpResponseRedirect('#')
    return render(request, 'bedriftskontakt/change_company.html', {'form': form, 'company': company})
"""


def has_BK_admin_permission(user):  # Used to check if user is in correct group
    permitted_groups = ['BedriftskontaktAdmin']
    if user.is_superuser:
        return True
    else:
        for group in permitted_groups:
            if user.groups.filter(name=group).exists():
                return True
        return False


def has_BK_permission(user):  # Used to check if user is in correct group
    permitted_groups = ['Bedriftskontakt', 'BedriftskontaktAdmin']
    if user.is_superuser:
        return True
    else:
        for group in permitted_groups:
            if user.groups.filter(name=group).exists():
                return True
        return False


def company_log(request, pk):
    company = get_object_or_404(Company, pk=pk)
    queryset = CompanyLog.objects.all().order_by('-pk')
    user = request.user
    semester_list = Semester.objects.all().order_by('-pk')
    if has_BK_permission(user):
        context = {
            'company': company,
            'company_log_list': queryset,
            'user': user,
            'semester_list': semester_list,
        }
        return render(request, 'bedriftskontakt/company_log.html', context)
    else:
        raise PermissionDenied


def company_log_create(request, pk):
    company = get_object_or_404(Company, pk=pk)
    form = CompanyLogAddForm(instance=company)
    user = request.user
    timeStamp = timezone.now()
    semester = Semester.objects.last()
    if has_BK_permission(user):
        if request.method == 'POST':
            form = CompanyLogAddForm(
                log_user=user, log_timeStamp=timeStamp, company=company, semester=semester, data=request.POST)
            form.save()
            return HttpResponseRedirect(reverse('bedriftskontakt:company_log', args=(pk,)))
        context = {
            'form': form,
            'company': company,
            'semester': semester,
        }
        return render(request, 'bedriftskontakt/company_log_add.html', context)
    else:
        raise PermissionDenied


def call_list(request, slug):
    semester = get_object_or_404(Semester, slug=slug)
    call_list = CallList.objects.filter(semester=semester)
    filter = CallListFilter(request.GET, queryset=call_list)
    company_log_dict = {
        "company": [],
        "log_status": [],
    }
    company_log = CompanyLog.objects.filter(semester=semester).order_by('-log_timeStamp')
    for obj in company_log:
        if obj.company not in company_log_dict["company"]:
            company_log_dict["company"].append(obj.company)
            company_log_dict["log_status"].append(obj.log_status)
    company_log_list_all = []
    i = 0
    for obj in call_list:
        if obj.company in company_log_dict["company"]:
            company_log_list_all.append(company_log_dict["log_status"][company_log_dict["company"].index(obj.company)])
            i += 1
        else:
            company_log_list_all.append("Ikke kontaktet")
    user = request.user
    if has_BK_permission(user):
        context = {
            'semester': semester,
            'call_list': call_list,
            'filter': filter,
            'company_log': company_log,
            'user': user,
            'company_log_list_all': company_log_list_all,
        }
        return render(request, 'bedriftskontakt/call_list.html', context)
    else:
        raise PermissionDenied


def personal_call_list(request, slug):
    user = request.user
    semester = get_object_or_404(Semester, slug=slug)
    call_list = CallList.objects.filter(semester=semester, company_asignee=user)
    company_log_dict = {
        "company": [],
        "log_status": [],
    }
    company_log = CompanyLog.objects.filter(semester=semester).order_by('-log_timeStamp')
    for obj in company_log:
        if obj.company not in company_log_dict["company"]:
            company_log_dict["company"].append(obj.company)
            company_log_dict["log_status"].append(obj.log_status)
    company_log_list_all = []
    i = 0
    for obj in call_list:
        if obj.company in company_log_dict["company"]:
            company_log_list_all.append(company_log_dict["log_status"][company_log_dict["company"].index(obj.company)])
            i += 1
        else:
            company_log_list_all.append("Ikke kontaktet")
    if has_BK_permission(user):
        context = {
            'semester': semester,
            'call_list': call_list,
            'company_log': company_log,
            'user': user,
            'company_log_list_all': company_log_list_all,
        }
        return render(request, 'bedriftskontakt/personal_call_list.html', context)
    else:
        raise PermissionDenied


def call_list_admin(request, slug):
    CallListFormSet = inlineformset_factory(Semester, CallList, fields=(
         'company_asignee', 'deadline',), extra=0, can_delete=False)
    company_list = Company.objects.all()
    semester = get_object_or_404(Semester, slug=slug)
    call_list = CallList.objects.filter(semester=semester)
    filter = CallListFilter(request.GET, queryset=call_list)
    formset = CallListFormSet(instance=semester)
    form = NewSemesterForm
    form_deadline = NewDeadlineForm
    user = request.user
    semester_list = Semester.objects.all()
    if has_BK_admin_permission(user):
        if request.method == 'POST' and 'asigneebtn' in request.POST:
            formset = CallListFormSet(request.POST, instance=semester)
            call_list = CallList.objects.filter(semester=semester)
            filter = CallListFilter(request.GET, queryset=call_list)
            formset.save()
            return HttpResponseRedirect(reverse('bedriftskontakt:call_list_admin', args=(slug,)))
        if request.method == 'POST' and 'newsemesterbtn' in request.POST:
            form = NewSemesterForm(request.POST)
            form.save()
            newSemester = Semester.objects.last()
            for company in company_list:
                CallList.objects.create_CallList(company=company, semester=newSemester)
            return HttpResponseRedirect(reverse('bedriftskontakt:call_list_admin', args=(newSemester.slug,)))
        if request.method == 'POST' and 'newdeadline' in request.POST:
            form_deadline = NewDeadlineForm(request.POST)
            if form_deadline.is_valid():
                deadline = form_deadline.cleaned_data['deadline']
                CallList.objects.change_deadline(deadline)
                return HttpResponseRedirect(reverse('bedriftskontakt:call_list_admin', args=(slug,)))
        context = {
            'call_list': call_list,
            'filter': filter,
            'company_list': company_list,
            'semester': semester,
            'formset': formset,
            'form': form,
            'semester_list': semester_list,
            'form_deadline': form_deadline,
            }
        return render(request, 'bedriftskontakt/call_list_admin.html', context)
    else:
        raise PermissionDenied


def create_company(request):
    form = CompanyCreationForm
    user = request.user
    if has_BK_permission(user):
        if request.method == 'POST':
            form = CompanyCreationForm(request.POST, request.FILES)
            form.save()
            newcompany = Company.objects.order_by('pk').last()
            semester = Semester.objects.last()
            CallList.objects.create_CallList(company=newcompany, semester=semester)
            return HttpResponseRedirect('/bedriftskontakt/bedrifter/')
        return render(request, 'bedriftskontakt/company/create_company.html', {'form': form})
    else:
        raise PermissionDenied


def edit_company(request, pk):
    company = get_object_or_404(Company, pk=pk)
    form = CompanyCreationForm(instance=company)
    user = request.user
    if has_BK_permission(user):
        if request.method == 'POST':
            form = CompanyCreationForm(request.POST, request.FILES, instance=company)
            form.save()
            return HttpResponseRedirect(reverse('bedriftskontakt:company_details', args=(pk,)))
        return render(request, 'bedriftskontakt/company/edit_company.html', {'form': form})
    else:
        raise PermissionDenied


def delete_company(request, pk):
    if request.method == 'POST':
        user = request.user
        if has_BK_admin_permission(user):
            company = get_object_or_404(Company, pk=pk)
            company.delete()
            return HttpResponseRedirect(reverse('bedriftskontakt:companyoverview'))
        else:
            raise PermissionDenied
    else:
        raise Http404


def team_bk_view(request):
    queryset = Member.objects.order_by("member_priority")
    context = {
        'members': queryset,
    }
    return render(request, 'bedriftskontakt/team_bk.html', context)


def business_presentations_view(request):
    event_list = Event.objects.filter(
        start_time__gte=timezone.now()).filter(business_presentation=True).order_by("start_time")

    if len(event_list) > 0:
        event = event_list[0]
        registered = event.attendees.all()
        user = request.user
        context = {}
        attendees = registered[:event.max_attendees]
        waiting = registered[event.max_attendees:]

        if request.method == 'POST':
            if "attending" in request.POST:
                event.attendees.add(user)
                return HttpResponseRedirect(reverse('bedriftskontakt:business_presentations'))
            elif "not_attending" in request.POST:
                event.attendees.remove(user)
                return HttpResponseRedirect(reverse('bedriftskontakt:business_presentations'))

        context = {'event_list': event_list, 'event': event, 'attendees': attendees, 'user': user, 'waiting': waiting}

        if event.registration:
            context['open'] = bool(event.registration_starts < timezone.now() < event.registration_ends)
            context['closed'] = bool(timezone.now() > event.registration_ends)
        return render(request, 'bedriftskontakt/business_presentations.html', context)
    else:
        return render(request, 'bedriftskontakt/business_presentations.html')


def ad_single_view(request, pk):
    ad = get_object_or_404(JobAd, pk=pk)
    context = {
        'ad': ad,
    }
    return render(request, 'bedriftskontakt/ad/ad_single.html', context)


def ad_list_view(request):
    ad_list = JobAd.objects.filter(Q(deadline__isnull=True) | Q(
        deadline__gte=datetime.today())).order_by('-priority_BK', F('deadline').asc(nulls_last=True))
    filter = JobAdFilter(request.GET, queryset=ad_list)
    today = datetime.today().date
    context = {
        'ad_list': ad_list,
        'filter': filter,
        'today': today,
    }
    return render(request, 'bedriftskontakt/ad/ad_list.html', context)


def ad_create_view(request):
    form = JobAdForm
    user = request.user
    location_form = LocationAddForm
    if has_BK_permission(user):
        if request.method == 'POST' and 'newadbtn' in request.POST:
            form = JobAdForm(request.POST)
            form.save()
            ad = JobAd.objects.latest('pk')
            pk = ad.pk
            return HttpResponseRedirect(reverse('bedriftskontakt:ad_single', args=(pk,)))
        if request.method == 'POST' and 'newlocationbtn' in request.POST:
            location_form = LocationAddForm(request.POST)
            location_form.save()
            return HttpResponseRedirect(reverse('bedriftskontakt:ad_create'))
        return render(request, 'bedriftskontakt/ad/create_ad.html', {'form': form, 'location_form': location_form})
    else:
        raise PermissionDenied


def change_ad(request, pk):
    ad = get_object_or_404(JobAd, pk=pk)
    form = JobAdForm(instance=ad)
    user = request.user
    location_form = LocationAddForm
    if has_BK_permission(user):
        if request.method == 'POST' and 'newadbtn' in request.POST:
            form = JobAdForm(data=request.POST, instance=ad)
            form.save()
            return HttpResponseRedirect(reverse('bedriftskontakt:ad_single', args=(pk,)))
        if request.method == 'POST' and 'newlocationbtn' in request.POST:
            location_form = LocationAddForm(request.POST)
            location_form.save()
            return HttpResponseRedirect(reverse('bedriftskontakt:change_ad', args=(pk,)))
        context = {
            'form': form,
            'location_form': location_form,
            'ad': ad
        }
        return render(request, 'bedriftskontakt/ad/change_ad.html', context)
    else:
        raise PermissionDenied


def ad_delete_view(request, pk):
    if request.method == 'POST':
        user = request.user
        if has_BK_permission(user):
            ad = get_object_or_404(JobAd, pk=pk)
            ad.delete()
            return HttpResponseRedirect(reverse('bedriftskontakt:ad_list'))
        else:
            raise PermissionDenied
    else:
        raise Http404


def Member_create_view(request):
    form = MemberAddForm
    user = request.user
    if has_BK_permission(user):
        if request.method == 'POST':
            form = MemberAddForm(request.POST, request.FILES)
            form.save()
            return HttpResponseRedirect('/bedriftskontakt/Team-BK/')
        return render(request, 'bedriftskontakt/create_member.html', {'form': form})
    else:
        raise PermissionDenied


def companyoverview(request):
    company_list = Company.objects.order_by("company_name")
    user = request.user
    if has_BK_permission(user):
        context = {
            'company_list': company_list,
        }
        return render(request, 'bedriftskontakt/company/companyoverview.html', context)
    else:
        raise PermissionDenied


def company_details(request, pk):
    company = get_object_or_404(Company, pk=pk)
    user = request.user
    if has_BK_permission(user):
        return render(request, 'bedriftskontakt/company/company_details.html', {'company': company})
    else:
        raise PermissionDenied


def for_company_view(request):
    return render(request, 'bedriftskontakt/for_company.html')


def for_student_view(request):
    return render(request, 'bedriftskontakt/for_student.html')

from .models import Semester
from .models import JobAd
from datetime import datetime
from django.db.models import Q, F


def navigation(request):
    semester_last = Semester.objects.last()
    return {'semester_last': semester_last}


def jobads(request):

    jobads_list = JobAd.objects.filter(Q(deadline__isnull=True) | Q(
        deadline__gte=datetime.today())).order_by('-priority_Mannhullet', F('deadline').asc(nulls_last=True))[:5]

    return {'jobads_list': jobads_list}

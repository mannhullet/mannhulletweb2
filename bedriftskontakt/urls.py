from django.urls import path
from . import views

app_name = 'bedriftskontakt'
urlpatterns = [


    path('bedriftspresentasjoner/', views.business_presentations_view, name='business_presentations'),
    path('stillingsannonser/', views.ad_list_view, name='ad_list'),
    path('stillingsannonser/<int:pk>/', views.ad_single_view, name='ad_single'),
    path('stillingsannonser/ny/', views.ad_create_view, name='ad_create'),
    path('annonser/<int:pk>/delete/', views.ad_delete_view, name='ad_delete'),
    path('for-bedriften/', views.for_company_view, name='for_company'),
    path('for-studenten/', views.for_student_view, name='for_student'),
    path('Team-BK/ny/', views.Member_create_view, name='member_create'),
    path('Team-BK/', views.team_bk_view, name='team_bk'),
    path('bedrifter/ny/', views.create_company, name='create_company'),
    path('bedrifter/<int:pk>/endre/', views.edit_company, name='edit_company'),
    path('bedrifter/<int:pk>/slett/', views.delete_company, name='delete_company'),
    path('bedrifter/', views.companyoverview, name='companyoverview'),
    path('bedrifter/<int:pk>/', views.company_details, name='company_details'),
    path('bedrifter/<int:pk>/logg/', views.company_log, name='company_log'),
    path('bedrifter/<int:pk>/logg/ny/', views.company_log_create, name='company_log_create'),
    path('ringeliste/admin/<slug:slug>/', views.call_list_admin, name='call_list_admin'),
    path('ringeliste/<slug:slug>/', views.call_list, name='call_list'),
    path('ringeliste/personlig/<slug:slug>/', views.personal_call_list, name='personal_call_list'),
    path('stillingsannonser/<int:pk>/endre/', views.change_ad, name='change_ad'),
]

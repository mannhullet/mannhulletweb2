# Generated by Django 2.2.10 on 2020-11-06 14:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bedriftskontakt', '0005_auto_20201103_1954'),
    ]

    operations = [
        migrations.AddField(
            model_name='calllist',
            name='deadline',
            field=models.DateField(blank=True, null=True),
        ),
    ]

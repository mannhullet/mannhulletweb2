# Generated by Django 2.2.10 on 2021-01-19 12:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bedriftskontakt', '0012_merge_20201113_0845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calllist',
            name='company_asignee',
            field=models.ForeignKey(blank=True, limit_choices_to={'is_staff': True}, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]

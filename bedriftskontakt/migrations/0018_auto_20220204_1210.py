# Generated by Django 2.2.17 on 2022-02-04 11:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bedriftskontakt', '0017_auto_20220128_1316'),
    ]

    operations = [
        migrations.RenameField(
            model_name='jobad',
            old_name='priority',
            new_name='priority_BK',
        ),
        migrations.AddField(
            model_name='jobad',
            name='priority_Mannhullet',
            field=models.BooleanField(default=False),
        ),
    ]

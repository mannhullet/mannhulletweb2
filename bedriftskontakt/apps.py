from django.apps import AppConfig


class BedriftskontaktConfig(AppConfig):
    name = 'bedriftskontakt'

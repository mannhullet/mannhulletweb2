Clone repository git:

 1. Last ned git fra https://git-scm.com/downloads
 2. [Add SSH-nøkkel på gitlab](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)
 3. Kopier SSH-adresse på gitLab
 4. Gå inn i terminal (git-bash)
 5. Lag en mappe som heter "www". Naviger til denne mappen
 6. `git clone git@gitlab.com:mannhullet/mannhulletweb2.git`
 7. Lage virtualenv:
      Windows:
      ```
      py -m venv venv
      ```
      Linux/OSX:
      ```
      python3 -m venv venv
      ```
 8. Aktivere virtualenv:
     Windows:
     ```
     venv\Scripts\activate
     ```
     Linux/OSX:
     ```
    source venv/bin/activate
    ```
 9. Lag en mappe som heter "mannhulletweb2" inne i "www". Naviger til denne mappen
 10. Installere requirements:
     ```
     pip install -r requirements/dev.txt
     ```
 11. Spesifiser at django skal bruke development settings som default:

     Linux/OSX:
     ```
     export DJANGO_SETTINGS_MODULE=mannhulletweb2.settings.dev
     ```

     Du kan også spesifisere det enkeltvis når du kaller `manage.py` med
     `--settings` flagget (PS! Hvis du kopierer denne og limer inn kan dobbel bindestrek ofte endres til en lang bindestrek):
     ```
     python3 manage.py runserver --settings mannhulletweb2.settings.dev
     ```
 12. Migrate for å opprette database:
     Windows:
     ```
     py manage.py migrate --settings mannhulletweb2.settings.dev
     ```
     Linux/OSX:
     ```
     python3 manage.py migrate --settings mannhulletweb2.settings.dev
     ```
 13. Voila! Nå kan du kjøre server med `py manage.py runserver --settings mannhulletweb2.settings.dev`
 14. Deaktivere venv: `deactivate` (eller lukk kommanduvinduet)

For å åpne virtualenv igjen neste gang, naviger til riktig mappe og gjenta 8.
Dersom det er blitt gjort endringer i models, må man gjenta 10 for å endre databasen.

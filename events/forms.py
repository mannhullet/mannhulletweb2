from django import forms
from .models import Event, EventConfig
from django.contrib.admin import widgets
from users.models import Mannhullitt


class EventForm(forms.ModelForm):
    start_time = forms.SplitDateTimeField(
        required=False,
        widget=widgets.AdminSplitDateTime,
        label='Starttidspunkt')
    end_time = forms.SplitDateTimeField(
        required=False,
        widget=widgets.AdminSplitDateTime,
        label='Slutttidspunkt')
    registration_starts = forms.SplitDateTimeField(
        required=False,
        widget=widgets.AdminSplitDateTime,
        label='Påmelding starter')
    registration_ends = forms.SplitDateTimeField(
        required=False,
        widget=widgets.AdminSplitDateTime,
        label='Påmelding slutter')
    invited_years = forms.MultipleChoiceField(
        choices=Mannhullitt.YEAR_CHOICES,
        widget=forms.CheckboxSelectMultiple(),
        label='Inviterte')

    SPECIAL_EVENTS = (
        ('ingen', 'Ingen'),
        ('ruka', 'RUKA'),
        ('jubileum', 'Jubileum'),
        ('bedriftsdagen', 'Bedriftsdagen'),
    )

    special_event = forms.CharField(label='Velg spesielt arrangement',
                                    widget=forms.Select(choices=SPECIAL_EVENTS))

    def clean_invited_years(self):
        invited_years = self.cleaned_data['invited_years']
        return ''.join(invited_years)

    def clean(self):
        end_time = self.cleaned_data.get('end_time')
        start_time = self.cleaned_data.get('start_time')
        registration = self.cleaned_data.get('registration')
        registration_starts = self.cleaned_data.get('registration_starts')
        registration_ends = self.cleaned_data.get('registration_ends')

        if end_time is None:
            self._errors['end_time'] = self.error_class(['Slutttidspunkt må spesifiseres'])
        if start_time is None:
            self._errors['start_time'] = self.error_class(['Starttidspunkt må spesifiseres'])

        if end_time is not None and start_time is not None:
            if start_time > end_time:
                self._errors['start_time'] = self.error_class(['Starttidspunkt må være før avslutningstidspunkt'])

        if registration:
            if registration_starts is None:
                self._errors['registration_starts'] = self.error_class(
                    ['Starttidspunkt for påmelding må spesifiseres for påmeldingsarrangement'])
            if registration_ends is None:
                self._errors['registration_ends'] = self.error_class(
                    ['Avslutningstidspunkt for påmelding må spesifiseres for påmeldingsarrangement'])
            if self.cleaned_data.get('max_attendees') < 1:
                self._errors['max_attendees'] = self.error_class(
                    ['Maks deltagere må være større enn 0'])
            if registration_ends is not None and registration_starts is not None:
                if registration_starts > registration_ends:
                    self._errors['registration_ends'] = self.error_class(
                        ['Avslutningstidspunkt for påmelding må være etter starttidspunkt for påmelding'])
        else:
            self.cleaned_data['max_attendees'] = 0

        if self.cleaned_data.get('business_presentation'):
            if self.cleaned_data.get('company') is None:
                self._errors['company'] = self.error_class(
                    ['Bedrift må velges for bedriftspresentasjon'])

        return self.cleaned_data

    class Meta:
        model = Event
        fields = [
            'title',
            'description',
            'start_time',
            'end_time',
            'registration',
            'registration_starts',
            'registration_ends',
            'max_attendees',
            'location',
            'organizer',
            'invited_years',
            'business_presentation',
            'company',
            'special_event',
            'binding_registration',
        ]

        labels = {
            'title': ('Tittel'),
            'description': ('Beskrivelse'),
            'start_time': ('Starttidspunkt'),
            'end_time': ('Slutttidspunkt'),
            'registration': ('Påmeldingsarrangement?'),
            'registration_starts': ('Påmelding starter'),
            'registration_ends': ('Påmelding slutter'),
            'max_attendees': ('Maks deltagere'),
            'location': ('Sted'),
            'organizer': ('Arrangør'),
            'invited_years': ('Inviterte'),
            'business_presentation': ('Bedriftspresentasjon'),
            'company': ('Bedrift'),
            'binding_registration': ('Bindende påmelding'),
        }

        help_texts = {
            'binding_registration': 'Kun for betalende arrangementer',
        }

    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        EventConfig.get_solo()
        special_event = EventConfig.objects.values()
        if True in list(special_event[0].values())[1:]:
            return
        else:
            del self.fields['special_event']

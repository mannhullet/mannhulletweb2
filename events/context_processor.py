from .models import Event, EventConfig

from django.utils import timezone


def events(request):
    events_list = Event.objects.filter(
                    start_time__gte=timezone.now()).order_by('start_time')[:5]
    return {'events_list': events_list}


def special_events(request):
    return {'special_events': EventConfig.get_solo()}

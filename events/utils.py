import datetime
from calendar import HTMLCalendar
from .models import Event


class Calendar(HTMLCalendar):
    def __init__(self, year=None, month=None):
        self.year = year
        self.month = month
        super(Calendar, self).__init__()

    # formats a day as a td
    # filter events by day
    def formatday(self, day, events):
        events_per_day = (events.filter(start_time__day=day) | events.filter(end_time__day=day) | events.filter(
            start_time__day__lte=day).filter(end_time__day__gte=day)).order_by('start_time')
        d = ''
        for ev in events_per_day:
            pk = str(ev.pk)
            # Need a way to get the complete url instead of writing it explicit
            url = "/arrangementer/" + pk
            if ev.business_presentation is True:
                d += f'<li class="eventDay BP"><a href={url}>{ev.title}</a></li>'
            elif ev.special_event == 'ruka':
                d += f'<li class="eventDay RUKA"><a href={url}>{ev.title}</a></li>'
            elif ev.special_event == 'jubileum':
                d += f'<li class="eventDay Jubileum"><a href={url}>{ev.title}</a></li>'
            elif ev.special_event == 'bedriftsdagen':
                d += f'<li class="eventDay Bedriftsdagen"><a href={url}>{ev.title}</a></li>'
            else:
                d += f'<li class="eventDay normalEvent"><a href={url}>{ev.title}</a></li>'

        if day != 0:
            if ((self.year == datetime.date.today().year) and (self.month == datetime.date.today().month)
                    and (day == datetime.date.today().day)):
                return f"<td><span class='date today'>{day}</span><ul> {d} </ul></td>"
            else:
                return f"<td><span class='date'>{day}</span><ul> {d} </ul></td>"
        return '<td class="notInMonth"></td>'

    # formats a week as a tr
    def formatweek(self, theweek, events, week_num):
        week = ''
        for d, weekday in theweek:
            week += self.formatday(d, events)
        return f'<tr> <td> {week_num} </td> {week} </tr>'

    def formatmonth(self, withyear=True):
        events = Event.objects.filter(start_time__year=self.year, start_time__month=self.month)
        cal = ''
        for week in self.monthdays2calendar(self.year, self.month):
            # Use the first non-zero day in each week to determine the ISO week number
            first_day = next((day for day, _ in week if day != 0), 1)
            _, week_num, _ = datetime.date(self.year, self.month, first_day).isocalendar()
            cal += f'{self.formatweek(week, events, week_num)}\n'
        return f'<tbody> {cal} </tbody>'

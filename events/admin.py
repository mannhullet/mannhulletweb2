from django.contrib import admin
from .models import Event, Attendance, EventConfig


class EventAdmin(admin.ModelAdmin):
    list_display = ['title', 'start_time']


admin.site.register(Event, EventAdmin)


class AttendanceAdmin(admin.ModelAdmin):
    list_display = ['get_event', 'timestamp', 'get_user']

    def get_event(self, obj):
        return obj.event.title

    def get_user(self, obj):
        return obj.attendee.username


admin.site.register(Attendance, AttendanceAdmin)

admin.site.register(EventConfig)

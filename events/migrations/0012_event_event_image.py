# Generated by Django 2.2.16 on 2021-07-29 09:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0011_auto_20201003_1249'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='event_image',
            field=models.ImageField(null=True, upload_to='event-images'),
        ),
    ]

# Generated by Django 2.2.6 on 2020-08-30 11:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_auto_20200830_1219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='invited_years',
            field=models.CharField(default='1234569', max_length=10),
        ),
    ]

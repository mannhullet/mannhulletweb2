from django import template

register = template.Library()


@register.filter(name='has_event_permisson')
def has_event_permisson(user):  # Used to check if user is in correct group
    permitted_groups = ['Styret', 'Bedriftskontakt', 'Undergruppeadmin', 'Webgruppen']
    if user.is_superuser:
        return True
    else:
        for group in permitted_groups:
            if user.groups.filter(name=group).exists():
                return True
        return False

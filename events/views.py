from django.shortcuts import render, get_object_or_404
from django.utils import timezone
import datetime
import csv
from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.utils.safestring import mark_safe
from django.core.exceptions import PermissionDenied

from django.core.mail import send_mail
from django.template.loader import render_to_string
from mannhulletweb2.settings import base
from .forms import EventForm
from .models import Event, Attendance
from users.models import Mannhullitt
from .utils import Calendar


def send_waiting_list_email(user, event):
    # Create message container - the correct MIME type is multipart/alternative.
    sender = base.EMAIL_HOST_USER
    reciever = user.email

    url = f"mannhullet.no{reverse('events:event_details', args=(event.pk,))}"

    # Create the body of the message (a plain-text and an HTML version).
    text = f"""
        Hei {user.get_full_name()}!\n
        Du har nå fått plass på {event.title}!
        Dersom du ikke har mulighet til å delta, meld deg av på mannhullet.no
    """
    html = f'''
    <div style="font-size: 16px">
        <p>Hei {user.get_full_name()}!<br><br>
        Du har nå fått plass på {event.title}<br>
        Dersom du ikke har mulighet til å delta,
        meld deg av på <link href={url}>{url}<link>.
        </p>
    </div>
    '''

    send_mail(
        subject=f"Du har blitt tildelt plass på {event.title}",
        message=text,
        html_message=render_to_string('email_base.html', {'contents': html}),
        recipient_list=[reciever, ],
        from_email=sender,
        fail_silently=False,
    )


def send_registered_dot_email(user, event):
    # Create message container - the correct MIME type is multipart/alternative.
    sender = base.EMAIL_HOST_USER
    reciever = user.email
    dots = user.dots

    # Create the body of the message (a plain-text and an HTML version).
    text = f'''
        Hei {user.get_full_name()}!\n
        Du har fått en prikk fra følgene arrangement: {event.title}, og har nå {dots} prikk(er).
        Dersom du mener dette er feil kan du ta kontakt med bedriftskontakt@marin.ntnu.no
    '''
    html = f'''
    <div style="font-size: 16px">
        <p>Hei {user.get_full_name()}!<br><br>
        Du har fått en prikk fra følgende arrangement: {event.title}, og har nå {dots} prikk(er).<br>
        Dersom du mener dette er feil kan du ta kontakt med bedriftskontakt@marin.ntnu.no.
        </p>
    </div>
    '''

    send_mail(
        subject=f"Prikk tildelt grunnet {event.title}",
        message=text,
        html_message=render_to_string('email_base.html', {'contents': html}),
        recipient_list=[reciever, ],
        from_email=sender,
        fail_silently=False,
    )


def get_all_event_attendees(event):
    # Attendance objects are sorted by timestamp. This means
    # We have to get the Attendence objects first, and then
    # find the connected users through this.
    attendances = Attendance.objects.filter(event=event)
    return [attendance.attendee for attendance in attendances]


def has_event_permisson(user):  # Used to check if user is in correct group
    permitted_groups = ['Styret', 'Bedriftskontakt', 'Undergruppeadmin', 'Webgruppen']
    if user.is_superuser:
        return True
    else:
        for group in permitted_groups:
            if user.groups.filter(name=group).exists():
                return True
        return False


def events(request):
    events_list = Event.objects.filter(end_time__gte=timezone.now()).order_by('end_time')
    return render(request, 'events/events.html', {'events_list': events_list})


def create_event(request):
    form = EventForm
    user = request.user
    if has_event_permisson(user):
        if request.method == 'POST':
            form = EventForm(data=request.POST)
            if form.is_valid():
                form.save()
                event = Event.objects.latest('pk')
                pk = event.pk
                return HttpResponseRedirect(reverse('events:event_details', args=(pk,)))
            else:
                return render(request, 'events/create_event.html', {'form': form})
        return render(request, 'events/create_event.html', {'form': form})
    else:
        raise PermissionDenied


def change_event(request, pk):
    event = get_object_or_404(Event, pk=pk)
    attendees = Attendance.objects.filter(event=event)[:event.max_attendees]
    form = EventForm(instance=event)
    already_invited = []
    already_invited[:0] = event.invited_years
    form.initial['invited_years'] = already_invited
    user = request.user
    event_finished = bool(timezone.now() > event.end_time)
    context = {'form': form, 'event': event, "attendees": attendees, "event_finished": event_finished}
    if has_event_permisson(user):
        if request.method == 'POST':
            form = EventForm(data=request.POST, instance=event)
            if form.is_valid():
                if "submit" in request.POST:
                    form.save()
                    event.save()
                    return HttpResponseRedirect(reverse('events:event_details', args=(pk,)))
            else:
                context['form'] = form
                return render(request, 'events/change_event.html', context)
        return render(request, 'events/change_event.html', context)
    else:
        raise PermissionDenied


def event_details(request, pk):
    event = get_object_or_404(Event, pk=pk)
    user = request.user
    registered = get_all_event_attendees(event=event)
    attendees = registered[:event.max_attendees]
    waiting = registered[event.max_attendees:]
    if waiting:
        can_sign_off = event.start_time - datetime.timedelta(hours=8) > timezone.now()
    else:
        can_sign_off = event.start_time - datetime.timedelta(hours=8) > timezone.now() \
                       and not event.binding_registration

    context = {}
    if request.method == 'POST':
        if "attending" in request.POST:
            if event.business_presentation and user.dots >= 3:
                raise Http404
            event.attendees.add(user)
            return HttpResponseRedirect(reverse('events:event_details', args=(pk,)))
        elif "not_attending" in request.POST:

            # Checks if current time is past 12:00 day before start of event, and if so give dot
            start_time = event.start_time.time()
            late_register_time = event.start_time - datetime.timedelta(days=1) - \
                datetime.timedelta(hours=start_time.hour, minutes=start_time.minute) + datetime.timedelta(hours=12)

            if (late_register_time < timezone.now() and user in attendees and event.business_presentation):
                register_user_dot(event=event, email=user.email)

            # Checks if there are users on the waiting list
            if len(waiting) > 0 and user in attendees:
                send_waiting_list_email(event=event, user=waiting[0])

            event.attendees.remove(user)
            return HttpResponseRedirect(reverse('events:event_details', args=(pk,)))
    context = {'event': event, 'attendees': attendees, 'user': user, 'waiting': waiting, 'can_sign_off': can_sign_off}
    if event.registration:
        context['open'] = bool(event.registration_starts < timezone.now() < event.registration_ends)
        context['closed'] = bool(timezone.now() > event.registration_ends)
    return render(request, 'events/event_details.html', context)


def register_attendance(request, pk, email):
    event = get_object_or_404(Event, pk=pk)
    attendee = get_object_or_404(Mannhullitt, email=email)
    user = request.user
    if has_event_permisson(user):
        if request.method == 'POST':
            attendance = Attendance.objects.filter(event=event, attendee=attendee)[0]
            attendance.has_attended = True
            attendance.save()
            return HttpResponseRedirect(reverse('events:change_event', args=(pk,)))
        else:
            raise Http404
    else:
        raise PermissionDenied


def remove_attendance(request, pk, email):
    event = get_object_or_404(Event, pk=pk)
    attendee = get_object_or_404(Mannhullitt, email=email)
    user = request.user
    if has_event_permisson(user):
        if request.method == 'POST':
            attendance = Attendance.objects.filter(event=event, attendee=attendee)[0]
            attendance.has_attended = False
            attendance.save()
            return HttpResponseRedirect(reverse('events:change_event', args=(pk,)))
        else:
            raise Http404
    else:
        raise PermissionDenied


def register_user_dot(event, email):
    attendee = get_object_or_404(Mannhullitt, email=email)
    attendance = Attendance.objects.filter(event=event, attendee=attendee)[0]
    attendance.recieved_dot = True
    attendance.save()
    attendee.register_dot()
    send_registered_dot_email(event=event, user=attendee)


def register_dots(request, pk, email):
    event = get_object_or_404(Event, pk=pk)
    user = request.user
    if has_event_permisson(user):
        if request.method == 'POST':
            register_user_dot(event=event, email=email)
            return HttpResponseRedirect(reverse('events:change_event', args=(pk,)))
        else:
            raise Http404
    else:
        raise PermissionDenied


def delete_event(request, pk):
    if request.method == 'POST':
        user = request.user
        if has_event_permisson(user):
            event = get_object_or_404(Event, pk=pk)
            event.delete()
            return HttpResponseRedirect(reverse('events:events'))
        else:
            raise PermissionDenied
    else:
        raise Http404


def calendar_view(request, year=None, month=None):
    if year is None:
        year = timezone.now().year
    if month is None:
        month = timezone.now().month

    events_list = Event.objects.filter(start_time__gte=timezone.now()).order_by('start_time')
    date = datetime.date(year, month, 1)
    yearNow = datetime.date.today().year
    cal = Calendar(year, month)
    html_cal = cal.formatmonth(withyear=True)

    # Assign correct month
    prev_year = year
    next_year = year
    prev_month = month-1
    next_month = month+1
    if prev_month < 1:
        prev_month = 12
        prev_year -= 1
    if next_month > 12:
        next_month = 1
        next_year += 1

    # Convert months to Norwegian
    if date.strftime('%B') == 'January':
        displayMonth = 'januar'
    elif date.strftime('%B') == 'February':
        displayMonth = 'februar'
    elif date.strftime('%B') == 'March':
        displayMonth = 'mars'
    elif date.strftime('%B') == 'May':
        displayMonth = 'mai'
    elif date.strftime('%B') == 'June':
        displayMonth = 'juni'
    elif date.strftime('%B') == 'July':
        displayMonth = 'juli'
    elif date.strftime('%B') == 'October':
        displayMonth = 'oktober'
    elif date.strftime('%B') == 'December':
        displayMonth = 'desember'
    else:
        # April, August and November are the same.
        displayMonth = date.strftime('%B').lower()

    # Also include year if not looking at current next_year
    displayMonthYear = displayMonth
    if yearNow != year:
        displayMonthYear += " (" + date.strftime('%Y') + ")"

    context = {'events_list': events_list, 'calendar': mark_safe(html_cal), 'month': displayMonthYear,
               'prev_month': prev_month, 'next_month': next_month, 'prev_year': prev_year, 'next_year': next_year}

    return render(request, 'events/event_calendar.html', context)


def export_attendees(request, pk):
    event = get_object_or_404(Event, pk=pk)
    response = HttpResponse(content_type='text/csv; charset=iso-8859-1')
    response['Content-Disposition'] = f'attachment; filename="{event.title}_attendees.csv"'
    writer = csv.writer(response)
    writer.writerow(['Fått plass'])
    writer.writerow(['Fornavn', 'Etternavn', 'Email', 'Klassetrinn', 'Allergier'])
    for attendee in get_all_event_attendees(event=event)[:event.max_attendees]:
        writer.writerow([attendee.first_name, attendee.last_name, attendee.email, attendee.year, attendee.allergies])
    writer.writerow(['Venteliste'])
    writer.writerow(['Fornavn', 'Etternavn', 'Email', 'Klassetrinn', 'Allergier'])
    for attendee in get_all_event_attendees(event=event)[event.max_attendees:]:
        writer.writerow([attendee.first_name, attendee.last_name, attendee.email, attendee.year, attendee.allergies])
    writer.writerow(['Mail-liste (Ikke inkludert venteliste):'])
    writer.writerow([attendee.email for attendee in get_all_event_attendees(event=event)[:event.max_attendees]])
    writer.writerow(['Mail-liste (Venteliste):'])
    writer.writerow([attendee.email for attendee in get_all_event_attendees(event=event)[event.max_attendees:]])

    return response

from django.test import TestCase
from events.models import Event
from django.urls import reverse
from users.models import Mannhullitt
from django.utils import timezone
from django.contrib.auth.models import Group
# Create your tests here.


class EventNotLoggedInTests(TestCase):
    def setUp(self):
        self.data = {
            'title': 'Test',
            'location': 'Tyholt',
            'organizer': 'Webgruppa',
        }
        self.event = Event.objects.create(**self.data)

    def test_can_view_event(self):
        response = self.client.get(reverse('events:event_details', args=[self.event.pk]))
        self.assertEqual(response.status_code, 200)

    def test_no_sign_up_or_change_button(self):
        response = self.client.get(reverse('events:event_details', args=[self.event.pk]))
        self.assertNotContains(response, "Meld på")
        self.assertNotContains(response, "Endre")

    # removed temporarily - the sign up system needs work on this detail.
    # def test_sign_up_denied(self):
    #     data = {"attending": "attending"}
    #     response = self.client.post(reverse(
    #         'events:event_details',
    #         args=[self.event.pk]), data, content_type="application/x-www-form-urlencoded")
    #     self.assertEqual(response.status_code, 404)

    def test_change_denied(self):
        response = self.client.post(reverse('events:change_event', args=[self.event.pk]))
        self.assertEqual(response.status_code, 403)

    def test_delete_denied(self):
        response = self.client.post(reverse('events:delete_event', args=[self.event.pk]))
        self.assertEqual(response.status_code, 403)

    def test_returns_name(self):
        self.assertEqual('Test', str(self.event))


class EventLoggedInTests(TestCase):
    def setUp(self):
        self.data = {
            'title': 'Test',
            'location': 'Tyholt',
            'organizer': 'Webgruppa',
            'invited_years': [1, 2, 3, 4, 5, 6, 9],
            'registration': True,
            'registration_starts': timezone.now(),
            'registration_ends': timezone.now() + timezone.timedelta(days=1),
        }
        self.event = Event.objects.create(**self.data)
        self.credentials = {
            'username': 'testuser',
            'email': 'example@stud.ntnu.no',
            'first_name': 'Test',
            'last_name': 'Testesen',
            'allergies': '',
            'enrollment_year': 2018,
            'year': 3,
            'is_active': True}
        self.user = Mannhullitt.objects.create(**self.credentials)
        self.user.set_password('secret')
        self.user.save()

    def test_sign_up_button(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.get(reverse('events:event_details', args=[self.event.pk]))
        self.assertContains(response, "Meld på")

    def test_sign_up(self):
        data = {"attending": "attending"}
        response = self.client.post(
            reverse('events:event_details', args=[self.event.pk]),
            data, content_type="application/x-www-form-urlencoded")
        self.assertEqual(response.status_code, 200)

    def test_change_denied(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:change_event', args=[self.event.pk]))
        self.assertEqual(response.status_code, 403)

    def test_delete_denied(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:delete_event', args=[self.event.pk]))
        self.assertEqual(response.status_code, 403)

    def test_create_denied(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:create_event'), data={
                    'title': 'Test2',
                    'description': 'Testing',
                    'start_time': timezone.now() + timezone.timedelta(hours=2),
                    'end_time': timezone.now() + timezone.timedelta(hours=3),
                    'registration': True,
                    'registration_starts': timezone.now(),
                    'registration_ends': timezone.now() + timezone.timedelta(hours=1),
                    'max_attendees': 10,
                    'location': 'Tyholt',
                    'organizer': 'Webgruppa',
                    'invited_years': [1, 2, 3, 4, 5, 6, 9],
                    'business_presentation': False,
                    'company': '',
                    })
        self.assertEqual(response.status_code, 403)


class EventAdminTests(TestCase):
    def setUp(self):
        self.group = Group(name="Styret")
        self.group.save()
        self.data = {
            'title': 'Test',
            'location': 'Tyholt',
            'organizer': 'Webgruppa',
            'invited_years': [1, 2, 3, 4, 5, 6, 9],
            'registration': True,
            'registration_starts': timezone.now(),
            'registration_ends': timezone.now() + timezone.timedelta(days=1),
        }
        self.event = Event.objects.create(**self.data)
        self.credentials = {
            'username': 'testuser',
            'email': 'example@stud.ntnu.no',
            'first_name': 'Test',
            'last_name': 'Testesen',
            'allergies': '',
            'enrollment_year': 2018,
            'year': 3,
            'is_active': True}
        self.user = Mannhullitt.objects.create(**self.credentials)
        self.user.set_password('secret')
        self.user.groups.add(self.group)
        self.user.save()

    def test_sign_up_and_change_button(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.get(reverse('events:event_details', args=[self.event.pk]))
        self.assertContains(response, "Meld på")
        self.assertContains(response, "Endre")

    def test_change_success(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:change_event', args=[self.event.pk]))
        self.assertEqual(response.status_code, 200)

    def test_delete_success(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:delete_event', args=[self.event.pk]))
        self.assertEqual(response.status_code, 302)

    def test_create_event(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('events:create_event'), data={
            'title': 'Test2',
            'description': 'Testing',
            'start_time': timezone.now() + timezone.timedelta(hours=2),
            'end_time': timezone.now() + timezone.timedelta(hours=3),
            'registration': True,
            'registration_starts': timezone.now(),
            'registration_ends': timezone.now() + timezone.timedelta(hours=1),
            'max_attendees': 10,
            'location': 'Tyholt',
            'organizer': 'Webgruppa',
            'invited_years': [1, 2, 3, 4, 5, 6, 9],
            'business_presentation': False,
            'company': '',
            })
        self.assertEqual(response.status_code, 200)

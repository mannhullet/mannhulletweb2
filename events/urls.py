from django.urls import path
from . import views

app_name = 'events'
urlpatterns = [
    path('', views.calendar_view, name='calendar'),
    path('<int:year>/<int:month>/', views.calendar_view, name="calendar_month"),
    path('<int:pk>/slett/', views.delete_event, name='delete_event'),
    path('opprett/', views.create_event, name='create_event'),
    path('<int:pk>/endre/', views.change_event, name='change_event'),
    path('<int:pk>/', views.event_details, name='event_details'),
    path('<int:pk>/register/<str:email>', views.register_attendance, name='register_attendance'),
    path('<int:pk>/remove/<str:email>', views.remove_attendance, name='remove_attendance'),
    path('<int:pk>/register_dot/<str:email>', views.register_dots, name='register_dots'),
    path('<int:pk>/export_attendees', views.export_attendees, name='export_attendees'),
    path('liste/', views.events, name='events')
]

from django.db import models
from django.utils import timezone
from users.models import Mannhullitt
from bedriftskontakt.models import Company
from tinymce.models import HTMLField
from solo.models import SingletonModel


class Event(models.Model):
    title = models.CharField(max_length=90)
    description = HTMLField()
    start_time = models.DateTimeField(default=timezone.now)
    end_time = models.DateTimeField(default=timezone.now)
    registration = models.BooleanField(default=True)
    registration_starts = models.DateTimeField(default=timezone.now, blank=True, null=True)
    registration_ends = models.DateTimeField(default=timezone.now, blank=True, null=True)
    max_attendees = models.IntegerField(default=0)
    location = models.CharField(max_length=50)
    organizer = models.CharField(max_length=30)
    invited_years = models.CharField(max_length=10)
    attendees = models.ManyToManyField(Mannhullitt, through='Attendance')
    business_presentation = models.BooleanField(default=False)
    event_image = models.ImageField(upload_to='event-images', null=True, blank=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, blank=True, null=True)
    special_event = models.CharField(max_length=50, blank=True, null=True)
    binding_registration = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class Attendance(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='attendants')
    attendee = models.ForeignKey(Mannhullitt, on_delete=models.CASCADE, related_name='attending')
    timestamp = models.DateTimeField(default=timezone.now)
    has_attended = models.BooleanField(default=False)
    recieved_dot = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.event} - {self.attendee}"

    class Meta:
        ordering = ('timestamp',)


class EventConfig(SingletonModel):
    RUKA = models.BooleanField(default=False)
    Jubileum = models.BooleanField(default=False)
    Bedriftsdagen = models.BooleanField(default=False)

    def __str__(self):
        return "Event Admin"

from django.db import models
from django.utils import timezone
from tinymce.models import HTMLField
# import reportlab


class MarinaPaper(models.Model):
    title = models.CharField(max_length=100)
    date = models.DateTimeField(default=timezone.now)
    file = models.FileField()
    description = HTMLField()
    frontpage = models.ImageField(upload_to='marina_pics', null=True, blank=True)

    def __str__(self):
        return self.title


class MarinaPodcast(models.Model):
    title = models.CharField(max_length=100)
    date = models.DateTimeField(default=timezone.now)
    file = models.FileField()
    description = HTMLField()

    def __str__(self):
        return self.title

from django.contrib import admin

# Register your models here.

from .models import MarinaPaper, MarinaPodcast


admin.site.register(MarinaPaper)
admin.site.register(MarinaPodcast)

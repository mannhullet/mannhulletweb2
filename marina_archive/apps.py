from django.apps import AppConfig


class MarinaArchiveConfig(AppConfig):
    name = 'marina_archive'

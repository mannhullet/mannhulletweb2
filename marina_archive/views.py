from django.shortcuts import render
from .models import MarinaPodcast, MarinaPaper
from django.core.exceptions import PermissionDenied
from django.db.models.functions import ExtractYear
from django.utils import timezone


def upload_pdf(request):
    user = request.user
    if user.is_staff:
        if request.method == 'upload_pdf':
            MarinaPaper()
            return
        return
    else:
        raise PermissionDenied


def paper_archive(request):
    # Get the current year
    current_year = timezone.now().year

    # Filter papers from the last 5 years
    five_years_ago = current_year - 5

    # Get a list of distinct years for which you have papers in the last 5 years
    years = MarinaPaper.objects.filter(date__year__gte=five_years_ago) \
        .annotate(year=ExtractYear('date')) \
        .order_by('-year') \
        .values_list('year', flat=True) \
        .distinct()

    # Create a dictionary to group papers by year.
    papers_by_year = {
        year: MarinaPaper.objects.filter(date__year=year).order_by('-date')
        for year in years
    }

    # Filter out years with no papers
    papers_by_year = {year: papers for year, papers in papers_by_year.items() if papers}

    return render(request, 'Marina/paper.html', {'papers_by_year': papers_by_year})


def podcast_archive(request):
    podcast_list = MarinaPodcast.objects.order_by('-date')
    return render(request, 'Marina/podcast.html', {'podcast_list': podcast_list})


def paper_archive_filtered(request):
    start_year = request.GET.get('start_year')
    end_year = request.GET.get('end_year')

    if start_year and end_year:
        start_year = int(start_year)
        end_year = int(end_year)

        # Check if start year is greater than end year
        if start_year > end_year:
            # Swap values or return an error message
            start_year, end_year = end_year, start_year

        papers_by_year = {
            year: MarinaPaper.objects.filter(date__year=year).order_by('-date')
            for year in reversed(range(start_year, end_year + 1))
        }
    else:
        # Default to the last 5 years if no input is provided
        current_year = timezone.now().year
        five_years_ago = current_year - 5
        papers_by_year = {
            year: MarinaPaper.objects.filter(date__year=year).order_by('-date')
            for year in range(five_years_ago, current_year + 1)
        }

    # Filter out years with no papers
    papers_by_year = {year: papers for year, papers in papers_by_year.items() if papers}

    return render(request, 'Marina/paper.html', {'papers_by_year': papers_by_year})

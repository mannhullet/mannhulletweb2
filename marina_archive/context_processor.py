from .models import MarinaPaper


def latest_marinas(request):
    latest_marinas = MarinaPaper.objects.all()[:4]
    return {'latest_marinas': latest_marinas}

from django.urls import path
from . import views

app_name = 'marina_archive'
urlpatterns = [
    path('paper/', views.paper_archive, name='Marina_paper'),
    path('upload_pdf/', views.upload_pdf, name='upload_pdf'),
    path('paper/filtered/', views.paper_archive_filtered, name='paper_archive_filtered')
]

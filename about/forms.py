# forms.py

from django import forms
from .models import Referat


class ReferatForm(forms.ModelForm):
    class Meta:
        model = Referat
        fields = ['title', 'upload_date', 'pdf_file']
        labels = {
            'title': 'Tittel',
            'upload_date': 'Opplastningsdato',
            'pdf_file': 'PDF',
        }

from django.test import TestCase
from about.models import Groups, Position, board_member, undergroupleader, ITV


class GroupTest(TestCase):
    def setUp(self):
        self.data = {
            'group_name': 'Testgruppe',
            }
        self.group = Groups.objects.create(**self.data)

    def test_returns_name(self):
        self.assertEqual(self.group.group_name, str(self.group))


class PositionTest(TestCase):
    def setUp(self):
        self.data = {
            'title': 'Sprøytefører',  # test with ø
            }
        self.position = Position.objects.create(**self.data)

    def test_returns_title(self):
        self.assertEqual(self.position.title, str(self.position))


class BoardmemberTest(TestCase):
    def setUp(self):
        self.data = {
            'title': 'Sprøytefører',  # test with ø
            }
        self.position = Position.objects.create(**self.data)
        self.data = {
            'first_name': 'Max',
            'surname': 'Testing',
            'mail': 'maxtest@example.com',
            'position': self.position
            }
        self.board_member = board_member.objects.create(**self.data)

    def test_returns_position(self):
        self.assertEqual(str(self.position), str(self.board_member))


class undergroupleaderTest(TestCase):
    def setUp(self):
        self.data = {
            'group_name': 'Testgruppe',
            }
        self.group = Groups.objects.create(**self.data)
        self.data = {
            'first_name': 'Max',
            'surname': 'Testing',
            'mail': 'maxtest@example.com',
            'group': self.group
            }
        self.undergroupleader = undergroupleader.objects.create(**self.data)

    def test_returns_group(self):
        self.assertEqual(str(self.group), str(self.undergroupleader))


class ITVTest(TestCase):
    def setUp(self):
        self.data = {
            'first_name': 'Max',
            'surname': 'Testing',
            'mail': 'maxtest@example.com',
            'year': 5
            }
        self.ITV = ITV.objects.create(**self.data)

    def test_returns_name(self):
        self.assertEqual('Max Testing', str(self.ITV))

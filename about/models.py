from django.db import models


# Create your models here.

class Groups(models.Model):
    group_name = models.CharField(max_length=150)

    class Meta:
        verbose_name_plural = "Undergrupper"

    def __str__(self):
        return self.group_name


class Position(models.Model):
    title = models.CharField(max_length=150)

    class Meta:
        verbose_name_plural = "Stillinger"
        managed = True

    def __str__(self):
        return self.title


class LosenPosition(models.Model):
    title = models.CharField(max_length=150)

    class Meta:
        verbose_name_plural = "Losen"
        managed = True

    def __str__(self):
        return self.title


class board_member(models.Model):
    first_name = models.CharField(max_length=150, blank=False)
    surname = models.CharField(max_length=150, blank=False)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    mail = models.EmailField(max_length=150, blank=False)
    image = models.ImageField(upload_to='boardmembers', null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Styremedlemmer'

    def __str__(self):
        return str(self.position)


class undergroupleader(models.Model):
    first_name = models.CharField(max_length=150, blank=False)
    surname = models.CharField(max_length=150, blank=False)
    group = models.ForeignKey(Groups, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='undergroupleader',
                              null=True, blank=True)
    mail = models.EmailField(max_length=150, blank=False)

    class Meta:
        verbose_name_plural = 'Undergruppeledere'

    def __str__(self):
        return str(self.group)


class ITV(models.Model):
    first_name = models.CharField(max_length=150, blank=False)
    surname = models.CharField(max_length=150, blank=False)
    mail = models.EmailField(max_length=150, blank=False)
    image = models.ImageField(upload_to='ITV', null=True, blank=True)
    year = models.IntegerField(blank=False)

    class Meta:
        verbose_name_plural = 'ITV'

    def __str__(self):
        return str(f'{self.first_name} {self.surname}')


class losen_member(models.Model):
    first_name = models.CharField(max_length=150, blank=False)
    surname = models.CharField(max_length=150, blank=False)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    mail = models.EmailField(max_length=150, blank=False)
    image = models.ImageField(upload_to='losenmembers', null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Losenmedlemmer'

    def __str__(self):
        return str(self.position)


class Referat(models.Model):
    title = models.CharField(max_length=255)
    upload_date = models.DateField()
    pdf_file = models.FileField()

    def __str__(self):
        return self.title

from django.contrib import admin
from .models import undergroupleader, board_member, Position, Groups, ITV, losen_member, Referat

# Register your models here.

admin.site.register(undergroupleader)
admin.site.register(board_member)
admin.site.register(Position)
admin.site.register(Groups)
admin.site.register(ITV)
admin.site.register(losen_member)
admin.site.register(Referat)

from django.urls import path
from . import views

urlpatterns = [
    path('general', views.general_view, name='general_info'),
    path('statutter/', views.statutter_view, name='statutter'),
    path('undergrupper/', views.undergruppe_view, name='undergrupper'),
    path('styret/', views.styret_view, name='styret'),
    path('ITV_KTR/', views.ITVKTR_view, name='ITVKTR'),
    path('losen/', views.losen_view, name='losen'),
    path('referat/', views.ReferatListView.as_view(), name='referat'),
    path('referat/add/', views.ReferatCreateView.as_view(), name='referat-create'),
    path('offiserer/', views.offiserer_view, name='offiserer'),
    path('DNV/', views.DNV_view, name='DNV'),
    path('varslingsskjema/', views.notificationform_view, name='varslingsskjema')
]

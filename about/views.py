from django.shortcuts import render
from .models import undergroupleader, board_member, ITV, losen_member, Referat
from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import ReferatForm
from django.views.generic.edit import CreateView
from users.models import Mannhullitt


def general_view(request):
    return render(request, 'about/general.html')


def statutter_view(request):
    return render(request, 'about/statutter.html')


def styret_view(request):
    boardmember_list_unsorted = board_member.objects.all()

    # Define the custom order for sorting
    order = {
        "Formann": 1,
        "1. Styrmann": 2,
        "Navigatør": 3,
        "Bk sjef": 4,
        "Grunkemeister": 5,
        "Arrangementssjef": 6,
        "Blæstesjef": 7,
        "Kjellersjef": 8,
        "Sprøytefører": 9,
        "Arkivar": 10,
        "Jungmann": 11
    }

    # Sort the list based on the custom order, replace 'name' with the correct field
    boardmember_list = sorted(boardmember_list_unsorted, key=lambda x: order.get(x.position.title, 1000))

    return render(request, 'about/board.html', {'board_members': boardmember_list})


def undergruppe_view(request):
    groupleaders = undergroupleader.objects.all().order_by('group_id')
    return render(request, 'about/undergroup.html', {'leaders': groupleaders})


def ITVKTR_view(request):
    itvs = ITV.objects.all()
    return render(request, 'about/ITVKTR.html', {'ITVs': itvs})


def DNV_view(request):
    return render(request, 'about/DNV.html')


@login_required(login_url='/brukere/login/')
def notificationform_view(request):
    return render(request, 'about/varslingsskjema.html')


def losen_view(request):
    losen_members = losen_member.objects.all()
    return render(request, 'about/losen.html', {'losen_members': losen_members})


class ReferatListView(LoginRequiredMixin, ListView):
    model = Referat
    template_name = 'about/referater.html'
    context_object_name = 'referater'
    queryset = Referat.objects.order_by('-upload_date')
    login_url = '/brukere/login/'  # You can specify the login url
    redirect_field_name = 'about/referater.html'


class ReferatCreateView(LoginRequiredMixin, CreateView):
    model = Referat
    form_class = ReferatForm
    template_name = 'about/add_referat.html'
    success_url = '/om/referat/'

    def form_valid(self, form):
        # form.instance.user = self.request.user  # Make sure this line is correct.
        # You need to have 'user' field in your model if you are going to use it.
        return super().form_valid(form)


def offiserer_view(request):
    # Define your custom order explicitly
    rank_order = ['Admiral', 'Æresadmiral', 'Viseadmiral', 'Kontreadmiral', 'Flagg-Kommandør']

    # Create an ordered dictionary with ranks as keys and empty list as values
    ordered_officers = {rank: [] for rank in rank_order}

    # Query once and categorize
    officers = Mannhullitt.objects.filter(rank__in=rank_order)
    for officer in officers:
        ordered_officers[officer.rank].append(officer)

    context = {'ordered_officers': ordered_officers}
    return render(request, 'about/offiserer.html', context)

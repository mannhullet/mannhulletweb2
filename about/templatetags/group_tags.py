from django import template
from itertools import groupby as it_groupby

register = template.Library()


@register.filter(name='has_group')
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists()


def get_attr(obj, attr):
    """Function to get attribute value from an object."""
    return getattr(obj, attr)


@register.filter
def groupby(queryset, field):
    """
    Groups a queryset by a specific field.
    """
    sorted_queryset = sorted(queryset, key=lambda x: get_attr(x, field))
    return [(key, list(group)) for key, group in it_groupby(sorted_queryset, lambda x: get_attr(x, field))]

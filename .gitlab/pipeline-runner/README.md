# GitLab pipeline runner setup

## Installation
Follow [instructions](https://docs.gitlab.com/runner/install/kubernetes.html)
to install runner in kubernetes cluster.

**NOTE:** The current `values.yaml` file is configured to create a service
account for the runner. You need the necessary permissions on the cluster for
creating a role in order for the installation to be successful.

The short version is to use these commands. This assumes the `pipeline-runner`
has already been created.

**Install new runner**
```
helm install --namespace pipeline-runner gitlab-runner -f values.yaml gitlab/gitlab-runner
```

**Upgrade existing runner**
```
helm upgrade --namespace pipeline-runner -f values.yaml gitlab-runner gitlab/gitlab-runner
```

**Delete the runner**
```
helm delete --namespace pipeline-runner gitlab-runner
```


## Configuration

Configuration is done by changing the values in `values.yaml`. The docs has a
section [Configuring GitLab Runner using the Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html#configuring-gitlab-runner-using-the-helm-chart).
In addition, [the docs for the kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html)
also contains information about which parameters can be changed.

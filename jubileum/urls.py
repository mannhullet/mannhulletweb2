from django.urls import path
from . import views


app_name = 'jubileum'
urlpatterns = [
   path('', views.home_view, name='jub-home'),
   path('events/', views.events_view, name='jub-events'),
   path('contact/', views.contact_view, name='jub-contact'),
   path('returnerende', views.returnerende_view, name='jub-returnerende'),
   path('samarbeidspartnere/', views.samarbeidspartner_view, name='jub-samarbeidspartnere'),
   path('funkeopptak/', views.funke_view, name='jub-funker'),
]

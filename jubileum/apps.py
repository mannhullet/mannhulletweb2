from django.apps import AppConfig


class JubileumConfig(AppConfig):
    name = 'jubileum'

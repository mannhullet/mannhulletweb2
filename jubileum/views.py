from django.shortcuts import render
from events.models import Event
from .forms import ImageForm

# Create your views here.


def home_view(request):
    return render(request, 'jubileum/jub-home.html')


def events_view(request):
    jub_events_list = Event.objects.filter(organizer='Jubileumskomiteen')
    # return render(request, 'jubileum/jub-event-placeholder.html', {'jub_events_list': jub_events_list})
    return render(request, 'jubileum/jub-events.html', {'jub_events_list': jub_events_list})


def contact_view(request):
    return render(request, 'jubileum/jub-contact.html')


def returnerende_view(request):
    """Process images uploaded by users"""
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # Get the current instance object to display in the template
            img_obj = form.instance
            return render(request, 'jubileum/jub-returnerende.html', {'form': form, 'img_obj': img_obj})
    else:
        form = ImageForm()
    return render(request, 'jubileum/jub-returnerende.html', {'form': form})


def samarbeidspartner_view(request):
    return render(request, 'jubileum/jub-samarbeidspartnere.html')


def funke_view(request):
    return render(request, 'jubileum/jub-funker.html')

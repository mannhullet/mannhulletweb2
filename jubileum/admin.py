from django.contrib import admin
from .models import oldImages


class JubileumsAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'image']


admin.site.register(oldImages, JubileumsAdmin)

from django import forms
from .models import oldImages
from django.core.exceptions import ValidationError


class ImageForm(forms.ModelForm):
    """Form for the image model"""
    class Meta:
        model = oldImages
        fields = ('title', 'description', 'image')

    def clean_image(self):
        image = self.cleaned_data.get('image', False)
        if image:
            if image.size > 1024*1024:
                raise ValidationError("Image file too large ( > 1 mb )")
            return image
        else:
            raise ValidationError("Couldn't read uploaded image")

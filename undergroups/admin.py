from django.contrib import admin
from .models import Undergroup, Sjiraffenvers, StudenterSang, FAQCategory, FAQItem

admin.site.register(Undergroup)


@admin.register(Sjiraffenvers)
class SjiraffenversAdmin(admin.ModelAdmin):
    list_display = ('title', 'creator', 'created_at')
    search_fields = ('title', 'creator__username')


class StudenterSangAdmin(admin.ModelAdmin):
    list_display = ('title', 'melody', 'creator')  # Fields to display in the admin list view
    search_fields = ('title', 'melody', 'creator')  # Fields to include in the search


admin.site.register(StudenterSang, StudenterSangAdmin)


class FAQItemInline(admin.TabularInline):
    model = FAQItem
    extra = 1


@admin.register(FAQCategory)
class FAQCategoryAdmin(admin.ModelAdmin):
    inlines = [FAQItemInline, ]

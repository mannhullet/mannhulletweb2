from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify
from tinymce.models import HTMLField


class Undergroup(models.Model):
    name = models.CharField(max_length=200)
    short_info = models.TextField(max_length=10000, blank=True)
    history = models.TextField(max_length=30000, blank=True)
    long_info = models.TextField(max_length=30000, blank=True)
    slug = models.SlugField(unique=True, null=False)
    logo = models.ImageField(upload_to='undergroups/logos', null=True, blank=True)
    cover_image = models.ImageField(upload_to='undergroups/coverimages', null=True, blank=True)

    class Meta():
        verbose_name_plural = "Undergrupper"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('undergroups:undergroup_page', kwargs={'slug': self.slug})

    def save(self, *args, **kwars):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwars)


class Sjiraffenvers(models.Model):
    title = models.CharField(max_length=255)  # Title of the verse
    line1 = models.CharField(max_length=255)  # First line of the verse
    line2 = models.CharField(max_length=255)  # Second line of the verse
    line3 = models.CharField(max_length=255)  # Third line of the verse
    line4 = models.CharField(max_length=255)  # Fourth line of the verse
    creator = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)  # Timestamp for creation

    def __str__(self):
        return self.title  # Display the title in admin

    def full_verse(self):
        # Combines all the lines into a full verse
        return "\n".join([self.line1, self.line2, self.line3, self.line4])

    class Meta:
        verbose_name_plural = "Sjiraffenvers"  # Correct plural spelling


class StudenterSang(models.Model):
    title = models.CharField(max_length=255)
    lyrics = models.TextField()
    melody = models.CharField(max_length=255, default="Ukjent", null=True)

    CREATOR_CHOICES = (
        ('Sanger av Tåkeluren', 'Sanger av Tåkeluren'),
        ('Annen Studentersang', 'Annen Studentersang'),
        ('Sanger av MiT', 'Sanger av MiT'),
        ('RUKA-sang', 'RUKA-sang'),
    )

    creator = models.CharField(max_length=50, choices=CREATOR_CHOICES, default='Tåkeluren')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('undergroups:song_detail', kwargs={'pk': self.pk})

    class Meta:
        verbose_name_plural = "Andre Sanger"  # Correct plural spelling


class FAQCategory(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class FAQFile(models.Model):
    file = models.FileField(upload_to='faq_files/')

    def __str__(self):
        return f"{self.file.name}"


class FAQItem(models.Model):
    category = models.ForeignKey(FAQCategory, related_name='faq_items', on_delete=models.CASCADE)
    subcategory = models.CharField(max_length=200)
    answer = HTMLField()
    files = models.ManyToManyField(FAQFile, related_name='faq_items', blank=True)

    def __str__(self):
        return self.subcategory

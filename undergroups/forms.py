from django import forms
from .models import Sjiraffenvers, FAQItem, FAQCategory, FAQFile


class SuggestSjiraffenversForm(forms.ModelForm):
    class Meta:
        model = Sjiraffenvers
        fields = ['title', 'line1', 'line2', 'line3', 'line4', 'creator']
        labels = {
            'title': 'Tittel',
            'line1': 'Linje 1',
            'line2': 'Linje 2',
            'line3': 'Linje 3',
            'line4': 'Linje 4',
            'creator': 'Forfatter',
        }


class FAQItemForm(forms.ModelForm):
    category = forms.ModelChoiceField(
        queryset=FAQCategory.objects.all(),
        empty_label="Choose Category",
        label='Kategori',
        widget=forms.Select(attrs={'class': 'form-control'})
    )
    subcategory = forms.CharField(
        label='Tittel',
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    answer = forms.CharField(
        label='Innhold',
        required=False,
        widget=forms.Textarea(attrs={'class': 'form-control tinymce'})
    )

    class Meta:
        model = FAQItem
        fields = ['category', 'subcategory', 'answer']

    def save(self, commit=True):
        instance = super(FAQItemForm, self).save(commit=False)

        if commit:
            instance.save()
            self.save_m2m()  # Save the many-to-many data for the form.

            # Handle the file upload here
            files = self.files.getlist('files')  # 'files' is the name of the form field for file uploads
            for f in files:
                faq_file = FAQFile(file=f)
                faq_file.save()
                instance.files.add(faq_file)  # Add the file to the many-to-many relationship

        return instance


class FAQCategoryForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Skriv inn ny kategori'}
    ), label='Kategori')

    class Meta:
        model = FAQCategory
        fields = ['name']

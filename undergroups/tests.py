from django.test import TestCase
from undergroups.models import Undergroup


class UndergroupTest(TestCase):
    def setUp(self):
        self.data = {
            'name': 'Testgruppe',
            'slug': 'test',
            }
        self.group = Undergroup.objects.create(**self.data)

    def test_returns_name(self):
        self.assertEqual(self.group.name, str(self.group))

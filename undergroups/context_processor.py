from .models import Undergroup


def navigation(request):
    group_list = Undergroup.objects.order_by('name')
    return {'group_list': group_list}

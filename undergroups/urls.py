from django.urls import path

from .views import Undergroup_detail_view, Undergroup_list_view, suggest_sjiraffenvers_view, faq_view, faq_item_create
from .views import sjiraffenvers_landing_view, SjiraffenversListView, StudenterSangListView, StudenterSangDetailView
from .views import faq_item_edit, faq_category_create

app_name = 'undergroups'
urlpatterns = [
    path('sjiraffenversgenerator-og-sang/', sjiraffenvers_landing_view, name='sjiraffenvers_main'),
    path('alle-sjiraffenvers/', SjiraffenversListView.as_view(), name='sjiraffenvers_list'),
    path('song/<int:pk>/', StudenterSangDetailView.as_view(), name='song_detail'),
    path('suggest-sjiraffenvers/', suggest_sjiraffenvers_view, name='suggest_sjiraffenvers'),
    path('faq/', faq_view, name='faq'),
    path('andre-sanger/', StudenterSangListView.as_view(), name='song_list'),
    path('', Undergroup_list_view.as_view(), name="undergroup_list"),
    path('<slug:slug>/', Undergroup_detail_view.as_view(), name="undergroup_page"),
    path('faq/add/', faq_item_create, name='faq_item_create'),
    path('faq/edit/<int:pk>/', faq_item_edit, name='faq_item_edit'),
    path('faq/category/add/', faq_category_create, name='faq_category_create'),
]

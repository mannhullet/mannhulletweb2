from django.apps import AppConfig


class UndergroupsConfig(AppConfig):
    name = 'undergroups'

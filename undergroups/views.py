import random
from itertools import groupby
from operator import attrgetter

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import DetailView, ListView

from .forms import SuggestSjiraffenversForm, FAQItemForm, FAQCategoryForm
from .models import Undergroup, Sjiraffenvers, StudenterSang, FAQCategory, FAQItem, FAQFile


class Undergroup_list_view(ListView):
    model = Undergroup
    context_object_name = 'group_list'
    template_name = 'undergroups/group_list.html'
    ordering = ["name"]


class Undergroup_detail_view(DetailView):
    model = Undergroup
    context_object_name = 'group'
    template_name = 'undergroups/base_groups.html'


@login_required
def sjiraffenvers_landing_view(request):
    verses = Sjiraffenvers.objects.all()
    random_verse = random.choice(verses) if verses.exists() else None
    return render(request, 'undergroups/tåkeluren/tåkeluren_main.html', {'random_verse': random_verse})


class SjiraffenversListView(LoginRequiredMixin, ListView):
    model = Sjiraffenvers
    context_object_name = 'verses'
    template_name = 'undergroups/tåkeluren/sjiraffenvers_list.html'
    ordering = ['title']  # Replace 'title' with the actual field name you want to sort by


class StudenterSangListView(LoginRequiredMixin, ListView):
    model = StudenterSang
    context_object_name = 'grouped_songs'
    template_name = 'undergroups/tåkeluren/song_list.html'

    def sort_grouped_songs(self, grouped_songs):
        creator_order = {key: i for i, (key, _) in enumerate(StudenterSang.CREATOR_CHOICES)}
        return sorted(grouped_songs, key=lambda x: creator_order.get(x[0], float('inf')))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        creators = groupby(self.get_queryset(), attrgetter('creator'))
        grouped_songs = [(creator, list(songs)) for creator, songs in creators]
        context['grouped_songs'] = self.sort_grouped_songs(grouped_songs)
        return context


class StudenterSangDetailView(LoginRequiredMixin, DetailView):
    model = StudenterSang
    template_name = 'undergroups/tåkeluren/song_detail.html'
    context_object_name = 'song'


@login_required
def suggest_sjiraffenvers_view(request):
    if request.method == 'POST':
        form = SuggestSjiraffenversForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('undergroups:sjiraffenvers_main')  # Redirect to a success page or the main page
    else:
        form = SuggestSjiraffenversForm()

    return render(request, 'undergroups/tåkeluren/suggest_sjiraffenvers.html', {'form': form})


def faq_view(request):
    faq_categories = FAQCategory.objects.all()
    return render(request, 'undergroups/faq/faq.html', {'faq_categories': faq_categories})


def is_styret_user(user):
    if user.groups.filter(name='Styret').exists():
        return True
    else:
        raise PermissionDenied


@login_required
@user_passes_test(is_styret_user)
def faq_item_create(request):
    if request.method == 'POST':
        form = FAQItemForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('undergroups:faq')
    else:
        form = FAQItemForm()

    return render(request, 'undergroups/faq/faq_item_form.html', {
        'form': form
    })


@login_required
@user_passes_test(is_styret_user)
def faq_item_edit(request, pk):
    faq_item = get_object_or_404(FAQItem, pk=pk)
    form = FAQItemForm(instance=faq_item)
    existing_files = faq_item.files.all()

    if request.method == 'POST':
        form = FAQItemForm(request.POST, request.FILES, instance=faq_item)
        if form.is_valid():
            form.save()

            # Handle deletion of files marked for deletion
            delete_files = request.POST.getlist('delete_files')  # 'delete_files' is the name of the checkbox input
            for file_id in delete_files:
                try:
                    file_to_delete = FAQFile.objects.get(id=file_id)
                    file_to_delete.delete()  # Delete the file from database and filesystem
                except FAQFile.DoesNotExist:
                    pass  # If file doesn't exist, do nothing

            return redirect('undergroups:faq')
    else:
        form = FAQItemForm(instance=faq_item)

    return render(request, 'undergroups/faq/faq_item_edit.html', {
        'form': form,
        'existing_files': existing_files,
        'faq_item': faq_item,
    })


@login_required
@user_passes_test(is_styret_user)
def faq_category_create(request):
    if request.method == 'POST':
        category_form = FAQCategoryForm(request.POST)
        if category_form.is_valid():
            category_form.save()
            return redirect('undergroups:faq_item_create')  # Redirect back to FAQ item creation
    else:
        category_form = FAQCategoryForm()

    return render(request, 'undergroups/faq/faq_category_form.html', {'category_form': category_form})

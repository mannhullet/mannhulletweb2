# Generated by Django 2.2.7 on 2020-04-15 12:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('undergroups', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='undergroup',
            options={'verbose_name_plural': 'Undergrupper'},
        ),
    ]

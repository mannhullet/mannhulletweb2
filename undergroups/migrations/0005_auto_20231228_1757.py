# Generated by Django 2.2.28 on 2023-12-28 16:57

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('undergroups', '0004_sjiraffenvers'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sjiraffenvers',
            name='creator',
            field=models.CharField(default=django.utils.timezone.now, max_length=255),
            preserve_default=False,
        ),
    ]

# Generated by Django 2.2.28 on 2023-12-28 16:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('undergroups', '0005_auto_20231228_1757'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sjiraffenvers',
            options={'verbose_name_plural': 'Sjiraffenvers'},
        ),
    ]

from django.shortcuts import render


def newstudent_view(request):
    return render(request, 'newstudent/ny_student.html')

from django.urls import path
from . import views

urlpatterns = [
    path('', views.newstudent_view, name='new_student')
]

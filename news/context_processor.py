from .models import NewsBanner


def banners(request):
    try:
        banners = NewsBanner.objects.filter(default=True).order_by('-date_created')[:5]
    except NewsBanner.DoesNotExist:
        banners = None
    return {'banners': banners}

from django.db import models
from tinymce.models import HTMLField
from django.urls import reverse
from events.models import Event
from django.utils.text import slugify
# Create your models here.


# Model for the new-banner
class NewsBanner(models.Model):
    title = models.CharField(max_length=200, null=False)
    picture = models.ImageField(upload_to='news/img', null=True, blank=False)
    ingress = models.TextField(max_length=1000, blank=True)

    link = models.SlugField(default=None, max_length=150, blank=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    default = models.BooleanField(default=False)
    article = HTMLField(blank=True)
    has_content = models.BooleanField(default=True)  # This need to be checked, or the banner will just be the picture
    date_created = models.DateTimeField(verbose_name='date created', auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.link:
            self.link = slugify(self.title)
            base_slug = self.link
            count = 1
            while NewsBanner.objects.filter(link=self.link).exclude(pk=self.pk).exists():
                self.link = f"{base_slug}-{count}"
                count += 1

        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('news:article_page', kwargs={'link': self.link})

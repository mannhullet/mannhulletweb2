from django.contrib import admin
from .models import NewsBanner

# Register your models here.


class BannerAdmin(admin.ModelAdmin):
    model = NewsBanner
    list_display = ['title', 'link']
    readonly_fields = ()
    exclude = ()
    fieldsets = ()
    filter_horizontal = ()
    list_filter = ()


admin.site.register(NewsBanner, BannerAdmin)

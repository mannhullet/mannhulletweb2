from django.urls import path
from .views import NewsArticleView
from . import views

app_name = 'news'
urlpatterns = [
    path('all/', views.news_all, name='all'),
    path('<slug:link>/', NewsArticleView.as_view(), name="article_page")
]

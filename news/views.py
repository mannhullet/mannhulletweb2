# from django.shortcuts import render
from django.views.generic import DetailView
from django.shortcuts import render
from .models import NewsBanner
# Create your views here.


class NewsArticleView(DetailView):
    model = NewsBanner
    context_object_name = 'banner'
    template_name = 'news/news_article.html'
    slug_field = 'link'
    slug_url_kwarg = 'link'


def news_all(request):
    return render(request, 'news/news_all.html')

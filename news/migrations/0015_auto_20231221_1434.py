# Generated by Django 2.2.28 on 2023-12-21 13:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0014_auto_20231221_1431'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsbanner',
            name='picture',
            field=models.ImageField(blank=True, null=True, upload_to='news/img'),
        ),
    ]

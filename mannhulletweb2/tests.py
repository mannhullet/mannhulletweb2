from django.test import TestCase
# Create your tests here.


class ErrorTests(TestCase):

    def test_404(self):
        response = self.client.get('/hei/')
        self.assertEqual(response.status_code, 404)

    def test_403(self):
        response = self.client.get('/arrangementer/opprett/')
        self.assertEqual(response.status_code, 403)


class rootTests(TestCase):

    def test_home(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

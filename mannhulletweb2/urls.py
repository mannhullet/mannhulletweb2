"""mannhulletweb2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView
from django.views.i18n import JavaScriptCatalog
from django.conf import settings
from django.conf.urls.static import static
from users.views import home_view, marin_hjelp

urlpatterns = [
    path('', home_view, name='home'),  # Created a view for homepage that is in the users app
    path('admin/', admin.site.urls),
    path('brukere/', include('users.urls')),
    path('brukere/', include('django.contrib.auth.urls')),
    path('arrangementer/', include('events.urls')),
    path('om/', include('about.urls')),
    path('bedriftskontakt/', include('bedriftskontakt.urls')),
    path('tinymce/', include('tinymce.urls')),
    path('undergrupper/', include('undergroups.urls')),
    path('utleie/', include('rental.urls')),
    path('varslingsskjema/', TemplateView.as_view(template_name='varslingsskjema.html'), name='varslingsskjema'),
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),
    path('news/', include('news.urls')),
    path('marina/', include('marina_archive.urls')),
    path('jubileum/', include('jubileum.urls')),
    path('nths/', include('nths.urls')),
    path('marin-hjelp', marin_hjelp, name='marin_hjelp'),
    path('nystudent/', include('new_student.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

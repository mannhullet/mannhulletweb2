"""
GoogleCloudStorage extensions suitable for handing Django's
Static and Media files.

These modified storage classes are needed because by default
the GoogleCloudStorage class is only able to be configured with
one bucket at the time (GS_BUCKET_NAME variable). Since two
buckets are needed, one for media and one for static files,
these modified classes are needed.

Requires following settings:
MEDIA_URL, GS_MEDIA_BUCKET_NAME
STATIC_URL, GS_STATIC_BUCKET_NAME

In addition to
https://django-storages.readthedocs.io/en/latest/backends/gcloud.html

Modified from:
https://github.com/jschneier/django-storages/issues/491#issue-317744052
"""
from django.conf import settings
from storages.backends.gcloud import GoogleCloudStorage
from storages.utils import setting


class GoogleCloudMediaStorage(GoogleCloudStorage):
    """GoogleCloudStorage suitable for Django's Media files."""

    def __init__(self, **kwargs):
        if not settings.MEDIA_URL:
            raise Exception('MEDIA_URL has not been configured')
        kwargs['bucket_name'] = setting('MW2_MEDIA_BUCKET_NAME')
        super(GoogleCloudMediaStorage, self).__init__(**kwargs)


class GoogleCloudStaticStorage(GoogleCloudStorage):
    """GoogleCloudStorage suitable for Django's Static files"""

    def __init__(self, **kwargs):
        if not settings.STATIC_URL:
            raise Exception('STATIC_URL has not been configured')
        kwargs['bucket_name'] = setting('MW2_STATIC_BUCKET_NAME')
        super(GoogleCloudStaticStorage, self).__init__(default_acl='publicRead', **kwargs)

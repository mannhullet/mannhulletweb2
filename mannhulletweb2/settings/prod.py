import os
import json
from mannhulletweb2.settings.base import *  # noqa: F401, F403
from google.oauth2.service_account import Credentials
from django.core.exceptions import ImproperlyConfigured

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['.mannhullet.no', '.mannhullet.com']

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('MW2_SECRET_KEY')

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('MW2_DB_NAME'),
        'USER': os.getenv('MW2_DB_USER'),
        'PASSWORD': os.getenv('MW2_DB_PASSWORD'),
        'HOST': os.getenv('MW2_DB_HOST'),
        'PORT': os.getenv('MW2_DB_PORT'),
    }
}

# Google ReCaptcha keys for mannhullet.com and staging.mannhullet.com

GOOGLE_RECAPTCHA_SECRET_KEY = os.getenv('MW2_RECAPTCHA_SECRET_KEY')
GOOGLE_RECAPTCHA_PUBLIC_KEY = os.getenv('MW2_RECAPTCHA_PUBLIC_KEY')

# Automatic Update DATABASE Database keys for mannhullet.com and staging.mannhullet.com
DATABASE_UPDATE_SECRET_KEY = os.getenv('DATABASE_UPDATE_SECRET_KEY')

# Static and media file storage
account_json = os.getenv('MW2_STORAGE_SERVICE_ACC')
if account_json is not None:
    account_dict = json.loads(account_json)
    account_credentials = Credentials.from_service_account_info(account_dict)
    GS_CREDENTIALS = account_credentials
else:
    raise ImproperlyConfigured("Missing environment variable: MW2_STORAGE_SERVICE_ACC")

GS_CACHE_CONTROL = 'max-age=86400'
GS_PROJECT_ID = os.getenv('MW2_GCP_PROJECT')

STATICFILES_STORAGE = 'mannhulletweb2.storages.gcloud.GoogleCloudStaticStorage'
MW2_STATIC_BUCKET_NAME = os.getenv('MW2_STATIC_BUCKET')

DEFAULT_FILE_STORAGE = 'mannhulletweb2.storages.gcloud.GoogleCloudMediaStorage'
MW2_MEDIA_BUCKET_NAME = os.getenv('MW2_MEDIA_BUCKET')

INSTALLED_APPS.insert(0, 'collectfast')  # noqa: F405
COLLECTFAST_STRATEGY = "collectfast.strategies.gcloud.GoogleCloudStrategy"

# Security settings
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

# Performance settings
CONN_MAX_AGE = 900

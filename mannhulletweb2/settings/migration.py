# This settings file should be used by the migration job that runs during
# deployment. It is a separate file so that not all settings that are used
# for production have to be added to the migration-job template as well as the
# deployment template. Since most of these settings are not used during
# migration anyways, having validation errors thrown is unnecessary.

import os
from mannhulletweb2.settings.base import *  # noqa: F401, F403

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('MW2_SECRET_KEY')

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('MW2_DB_NAME'),
        'USER': os.getenv('MW2_DB_USER'),
        'PASSWORD': os.getenv('MW2_DB_PASSWORD'),
        'HOST': os.getenv('MW2_DB_HOST'),
        'PORT': os.getenv('MW2_DB_PORT'),
    }
}

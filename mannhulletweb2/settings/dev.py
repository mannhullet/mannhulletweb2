import os
from mannhulletweb2.settings.base import *  # noqa: F403

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1']

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0i^$xlus*6yg2e4v(vyhvjihkrndn0tn^^&elgvj_u6f_yfqrk'

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases
if os.getenv('MW2_USE_POSTGRESQL', 'false') == 'true':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': os.getenv('MW2_DB_NAME', 'postgres'),
            'USER': os.getenv('MW2_DB_USER', 'postgres'),
            'PASSWORD': os.getenv('MW2_DB_PASSWORD', ''),
            'HOST': os.getenv('MW2_DB_HOST', 'db'),
            'PORT': os.getenv('MW2_DB_PORT', 5432),
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),  # noqa: F405
        }
    }


GOOGLE_RECAPTCHA_SECRET_KEY = '6Lf0arEZAAAAAKBm97FBI_KHhs1Aa0MRO0jf6gVn'
GOOGLE_RECAPTCHA_PUBLIC_KEY = "6Lf0arEZAAAAAJBBWLWbTETpa-PtbMuDyBJuuf7X"


MEDIA_ROOT = os.path.join(BASE_DIR, "media")  # noqa: F405

# Override with local settings, if they exist
try:
    from mannhulletweb2.settings.local import *  # noqa: F401, F403
except ImportError:
    pass

from django.shortcuts import render, get_object_or_404
from django.utils.safestring import mark_safe
from .models import RentalObject, RentalPeriod
from users.models import Mannhullitt
from .utils import RentalCalendarWeek, from_iso_calendar
from .forms import RentalPeriodForm
import datetime
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import HttpResponseForbidden
from mannhulletweb2.settings import base
from django.core.mail import send_mail
from django.template.loader import render_to_string


def send_notification_email(rental_period, renting_group_email):
    sender = base.EMAIL_HOST_USER
    reciever = renting_group_email
    subject = "Ny forespørsel om leie venter godkjennelse"
    message = f"""
        Det er en ny forespørsel om leie for {rental_period.rental_object}.
        Detaljene om leie er:\n
        Leietaker: {rental_period.renter_full_name()}\n
        Start Tid: {format_datetime_norwegian(rental_period.start_time)}\n
        Slutt Tid: {format_datetime_norwegian(rental_period.end_time)}\n
    """

    send_mail(
        subject=subject,
        message=message,
        from_email=sender,
        recipient_list=[reciever, ],
        fail_silently=False,
    )


def object_view(request, pk):
    rental_object = get_object_or_404(RentalObject, pk=pk)
    user = request.user
    if request.method == 'POST' and "submit" in request.POST:
        rental_period = RentalPeriod(rental_object=rental_object)
        form = RentalPeriodForm(request.POST, instance=rental_period)

        if form.is_valid():
            start_datetime = form.cleaned_data.get('start_time')
            end_datetime = form.cleaned_data.get('end_time')
            rented_objects_count = RentalPeriod.objects.filter(
                rental_object=rental_object,
                start_time__lt=end_datetime,
                end_time__gt=start_datetime
            ).count()

            if rented_objects_count < rental_object.quantity:
                rental_period = form.save(commit=False)
                rental_period.renter = get_object_or_404(Mannhullitt, email=user.email)
                rental_period.approved = False
                rental_period.save()
                return HttpResponseRedirect(reverse('rental_confirmation'))
            else:
                form.add_error(None, "Leie perioden er ikke ledig, sjekk utleiekalender.")
        else:
            form = RentalPeriodForm()

        return render(request, 'rental/rental_object.html', {'rental_object': rental_object, 'form': form})

    else:
        form = RentalPeriodForm()  # Create an empty form
        return render(request, 'rental/rental_object.html', {'rental_object': rental_object, 'form': form})


def rental_confirmation_view(request):
    # Fetch the latest rental period - this assumes it's the one just created
    # Caution: This may not be thread-safe depending on your server setup
    # and could potentially send notifications for the wrong rental if two are made near-simultaneously.
    rental_period = RentalPeriod.objects.latest('id')

    if rental_period.rental_object.renting_group in ['SBK', 'MBK']:
        renting_group_email = rental_period.rental_object.renting_group_email
        send_notification_email(rental_period, renting_group_email)

    return render(request, 'rental/rental_confirmation.html')


def approve_rental(request, pk):
    rental_period = get_object_or_404(RentalPeriod, pk=pk)
    rental_period.approved = True
    rental_period.save()


def all_view(request):
    objects = RentalObject.objects.all()
    context = {
        'coma_active': False,
        'mbk_active': False,
        'sbk_active': False,
        'board_active': False,
        'beer_active': False,
        'others_active': False,
        'all_active': True,
        'objects': objects
    }
    return render(request, 'rental/rental_page.html', context)


def COMA_view(request):
    objects = RentalObject.objects.all().filter(renting_group="COMA")
    context = {
        'coma_active': True,
        'mbk_active': False,
        'sbk_active': False,
        'board_active': False,
        'beer_active': False,
        'others_active': False,
        'all_active': False,
        'objects': objects
    }
    return render(request, 'rental/rental_page.html', context)


def MBK_view(request):
    objects = RentalObject.objects.all().filter(renting_group="MBK")
    context = {
        'coma_active': False,
        'mbk_active': True,
        'sbk_active': False,
        'board_active': False,
        'beer_active': False,
        'others_active': False,
        'all_active': False,
        'objects': objects
    }
    return render(request, 'rental/rental_page.html', context)


def SBK_view(request):
    objects = RentalObject.objects.all().filter(renting_group="SBK")
    context = {
        'coma_active': False,
        'mbk_active': False,
        'sbk_active': True,
        'board_active': False,
        'beer_active': False,
        'others_active': False,
        'all_active': False,
        'objects': objects
    }
    return render(request, 'rental/rental_page.html', context)


def board_view(request):
    objects = RentalObject.objects.all().filter(renting_group="BOARD")
    context = {
        'coma_active': False,
        'mbk_active': False,
        'sbk_active': False,
        'board_active': True,
        'beer_active': False,
        'others_active': False,
        'all_active': False,
        'objects': objects
    }
    return render(request, 'rental/rental_page.html', context)


def beer_view(request):
    objects = RentalObject.objects.all().filter(renting_group="BEER")
    context = {
        'coma_active': False,
        'mbk_active': False,
        'sbk_active': False,
        'board_active': False,
        'beer_active': True,
        'others_active': False,
        'all_active': False,
        'objects': objects
    }
    return render(request, 'rental/rental_page.html', context)


def others_view(request):
    objects = RentalObject.objects.all().filter(renting_group="OTHERS")
    context = {
        'coma_active': False,
        'mbk_active': False,
        'sbk_active': False,
        'board_active': False,
        'beer_active': False,
        'others_active': True,
        'all_active': False,
        'objects': objects
    }
    return render(request, 'rental/rental_page.html', context)


def get_month_name_from_date(date):
    # Mapping of English month names to Norwegian
    month_map = {
        1: 'Januar',
        2: 'Februar',
        3: 'Mars',
        4: 'April',
        5: 'Mai',
        6: 'Juni',
        7: 'Juli',
        8: 'August',
        9: 'September',
        10: 'Oktober',
        11: 'November',
        12: 'Desember'
    }

    return month_map[date.month]


def calendar_view(request, year=None, week=None):
    if year is None or week is None:
        today = datetime.date.today()
        year, week, _ = today.isocalendar()

    # Fetch the previous and next weeks
    prev_week = week - 1
    next_week = week + 1
    prev_year = year
    next_year = year

    if prev_week < 1:
        prev_week = 52
        prev_year -= 1

    if next_week > 52:
        next_week = 1
        next_year += 1

    start_of_week = from_iso_calendar(year, week, 1)
    month_name = get_month_name_from_date(start_of_week)

    # Get all RentalPeriods that are rented this week
    rental_periods_this_week = RentalPeriod.objects.filter(
        start_time__week=week,
        end_time__week=week,
        start_time__year=year,
        end_time__year=year
    )

    # Generate the calendar HTML
    cal = RentalCalendarWeek(year, week)
    html_cal = cal.formatweekview()

    context = {
        'calendar': mark_safe(html_cal),
        'rental_periods': rental_periods_this_week,
        'prev_week': prev_week,
        'next_week': next_week,
        'prev_year': prev_year,
        'next_year': next_year,
        'month_name': month_name,
        'current_week': week
    }
    return render(request, 'rental/calendar_page.html', context)


def unified_approval_view(request):
    renting_groups = []

    # Add SBK and/or MBK to renting_groups based on the user's group memberships
    if request.user.groups.filter(name='SBK').exists() or request.user.is_superuser:
        renting_groups.append('SBK')
    if request.user.groups.filter(name='MBK').exists() or request.user.is_superuser:
        renting_groups.append('MBK')

    # Return forbidden if the user does not belong to either SBK or MBK groups
    if not renting_groups:
        return HttpResponseForbidden("You don't have permission to access this page.")

    # Fetch the pending rentals for the groups the user is a part of
    pending_rentals = RentalPeriod.objects.filter(approved=False, rental_object__renting_group__in=renting_groups)

    if request.method == 'POST':
        if 'delete_id' in request.POST:
            rental_id = request.POST.get('delete_id')
            rental_period = RentalPeriod.objects.get(id=rental_id)

            # Send email notification
            send_deleted_rental_email(rental_period.renter, rental_period)

            rental_period.delete()
            return HttpResponseRedirect(reverse('admin_approval'))

        elif 'approve_id' in request.POST:
            rental_id = request.POST.get('approve_id')
            rental_period = RentalPeriod.objects.get(id=rental_id)
            rental_period.approved = True

            # Send email notification
            send_approved_rental_email(rental_period.renter, rental_period)

            rental_period.save()
            return HttpResponseRedirect(reverse('admin_approval'))

    return render(request, 'rental/admin_approval.html', {'pending_rentals': pending_rentals})


def format_datetime_norwegian(dt):
    day_without_leading_zero = str(int(dt.strftime('%d')))
    year = dt.strftime('%Y')
    hour_minute = dt.strftime('%H:%M')
    month_name_norwegian = get_month_name_from_date(dt)
    return f"{day_without_leading_zero}. {month_name_norwegian} {year}, {hour_minute}"


def send_approved_rental_email(user, rental_period):
    sender = base.EMAIL_HOST_USER
    reciever = user.email
    # Create the body of the message (a plain-text and an HTML version).
    text = f"""
        Hei {user.get_full_name()}!\n
        Din leie av {rental_period.rental_object} er godkjent for perioden
         {format_datetime_norwegian(rental_period.start_time)} og
         {format_datetime_norwegian(rental_period.end_time)}.
    """
    html = f'''
    <div style="font-size: 16px">
        <p>Hei {user.get_full_name()}!<br><br>
        Din leie av {rental_period.rental_object} er godkjent for perioden
         {format_datetime_norwegian(rental_period.start_time)} og
         {format_datetime_norwegian(rental_period.end_time)}.
        </p>
    </div>
    '''

    send_mail(
        subject="Leie Godkjent",
        message=text,
        html_message=render_to_string('email_base.html', {'contents': html}),
        recipient_list=[reciever, ],
        from_email=sender,
        fail_silently=False,
    )


def send_deleted_rental_email(user, rental_period):
    sender = base.EMAIL_HOST_USER
    reciever = user.email

    # Create the body of the message (a plain-text and an HTML version).
    text = f"""
        Hei {user.get_full_name()}!\n
        Din leie av {rental_period.rental_object} for perioden {format_datetime_norwegian(rental_period.start_time)} og
         {format_datetime_norwegian(rental_period.end_time)}
         har ikke blitt godkjent av admin. Mener du dette er feil, kontakt
          {rental_period.rental_object.renting_group_email}.
    """
    html = f'''
    <div style="font-size: 16px">
        <p>Hei {user.get_full_name()}!<br><br>
        Din leie av {rental_period.rental_object} for perioden {format_datetime_norwegian(rental_period.start_time)} og
         {format_datetime_norwegian(rental_period.end_time)}
         har ikke blitt godkjent av admin. Mener du dette er feil, kontakt
          {rental_period.rental_object.renting_group_email}.
        </p>
    </div>
    '''

    send_mail(
        subject="Leie Ikke Godkjent",
        message=text,
        html_message=render_to_string('email_base.html', {'contents': html}),
        recipient_list=[reciever, ],
        from_email=sender,
        fail_silently=False,
    )

from django.contrib import admin
from .models import RentalObject, RentalPeriod


class RentalObjectAdmin(admin.ModelAdmin):
    list_display = ['title', 'renting_group', 'quantity']


admin.site.register(RentalObject, RentalObjectAdmin)


class RentalPeriodAdmin(admin.ModelAdmin):
    list_display = ['rental_object', 'renter', 'approved', 'start_time', 'end_time']


admin.site.register(RentalPeriod, RentalPeriodAdmin)


# admin.site.register(Renting_group)

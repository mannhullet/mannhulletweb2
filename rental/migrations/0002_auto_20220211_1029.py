# Generated by Django 2.2.20 on 2022-02-11 09:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rental', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rentalobject',
            name='renting_group',
            field=models.CharField(choices=[('COMA', 'COMA'), ('MBK', 'MBK'), ('BEER', 'Ølbryggerlauget'), ('SBK', 'SBK'), ('BOARD', 'Styret'), ('OTHER', 'Annet')], max_length=7),
        ),
    ]

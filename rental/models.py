from django.db import models
from django.utils import timezone
from users.models import Mannhullitt
from tinymce.models import HTMLField
from django.core.exceptions import ValidationError


class RentalObject(models.Model):
    RENTING_GROUP_CHOICES = [
        ('COMA', 'COMA'),
        ('MBK', 'MBK'),
        ('BEER', 'Ølbryggerlauget'),
        ('SBK', 'SBK'),
        ('BOARD', 'Styret'),
        ('OTHERS', 'Andre'),
    ]

    RENTING_GROUP_EMAILS = {
        'COMA': 'coma@mannhullet.no',
        'MBK': 'mbk@mannhullet.no',
        'BEER': 'bryggelauget@mannhullet.no',
        'SBK': 'sbk@mannhullet.no',
        'BOARD': 'arkivar@mannhullet.no'
    }

    title = models.CharField(max_length=64)
    info = HTMLField()
    image = models.ImageField(upload_to='rentalobjects', null=True, blank=True)
    MBK_check = models.BooleanField(default=False)
    SBK_check = models.BooleanField(default=False)
    quantity = models.IntegerField(default=1)
    renting_group = models.CharField(max_length=7, choices=RENTING_GROUP_CHOICES)

    def __str__(self):
        return self.title

    @property
    def renting_group_email(self):
        return self.RENTING_GROUP_EMAILS.get(self.renting_group)


class RentalPeriod(models.Model):
    rental_object = models.ForeignKey(RentalObject, on_delete=models.CASCADE, related_name='rental_period')
    renter = models.ForeignKey(Mannhullitt, on_delete=models.CASCADE, related_name='renting')
    approved = models.BooleanField(default=False)
    start_time = models.DateTimeField(default=timezone.now)
    end_time = models.DateTimeField(default=timezone.now)
    note = models.TextField(blank=True, null=True)

    def clean(self):
        if self.approved and self.rental_object.renting_group not in ['MBK', 'SBK']:
            raise ValidationError("Bare MBK og SBK trenger å bli godkjent.")

    def renter_full_name(self):
        return self.renter.get_full_name()

    def __str__(self):
        st = self.start_time
        return f"{self.rental_object} - {self.renter} - {st.day}.{st.month}.{st.year}"

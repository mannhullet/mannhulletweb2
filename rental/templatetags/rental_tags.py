from django import template

register = template.Library()


@register.filter(name='has_rental_permission')
def has_rental_permission(user):
    # If the user is a superuser, return True
    if user.is_superuser:
        return True

    # Check if the user is in both the 'MBK' and 'SBK' groups
    is_in_MBK = user.groups.filter(name='MBK').exists()
    is_in_SBK = user.groups.filter(name='SBK').exists()

    return is_in_MBK and is_in_SBK


@register.filter(name='is_MBK')
def is_MBK(user):
    if user.is_superuser:
        return True
    else:
        return user.groups.filter(name='MBK').exists()


@register.filter(name='is_SBK')
def is_SBK(user):
    if user.is_superuser:
        return True
    else:
        return user.groups.filter(name='SBK').exists()

from django.urls import path
from . import views

urlpatterns = [
    path('alle/', views.all_view, name='all'),
    path('coma/', views.COMA_view, name='coma'),
    path('mbk/', views.MBK_view, name='mbk'),
    path('sbk/', views.SBK_view, name='sbk'),
    path('styret/', views.board_view, name='board'),
    path('olbryggerlauget/', views.beer_view, name='beer'),
    path('andre/', views.others_view, name='others'),
    path('<int:pk>/', views.object_view, name='object_view'),
    path('calendar/', views.calendar_view, name='calendar'),
    path('calendar/<int:year>/<int:week>/', views.calendar_view, name="calendar_week"),
    path('rental-confirmation/', views.rental_confirmation_view, name='rental_confirmation'),
    path('admin/approval/', views.unified_approval_view, name='admin_approval')
]

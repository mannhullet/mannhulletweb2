from django.test import TestCase
from django.utils import timezone
# from django.urls import reverse
from .models import RentalObject, RentalPeriod
from users.models import Mannhullitt


class RentalObjectTest(TestCase):
    def setUp(self):
        self.data = {
            'title': "Havfruen",
            'MBK_check': True,
            'SBK_check': False,
            'quantity': 1,
            'renting_group': 'MBK',
        }
        self.rental_object = RentalObject.objects.create(**self.data)

        self.credentials = {
            'username': 'testuser',
            'email': 'example@stud.ntnu.no',
            'first_name': 'Test',
            'last_name': 'Testesen',
            'allergies': '',
            'enrollment_year': 2018,
            'year': 3,
            'is_active': True
            }
        self.user = Mannhullitt.objects.create(**self.credentials)
        self.user.set_password('secret')
        self.user.save()

    # def test_delete_success(self):
    #     self.client.login(username='testuser', password='secret')
    #     response = self.client.post(reverse('rental/rentalobject:delete_rental_object', args=[self.rental_object.pk]))
    #     self.assertEqual(response.status_code, 302)

    # def test_create_rental_object(self):
    #     self.client.login(username='testuser', password='secret')
    #     response = self.client.post(reverse('rental/rentalobject:create_rental_object'), data={
    #         'title': "Havfruen",
    #         'info': "Havfruen er Mannhullets motorbåt",
    #         'MBK_check': True,
    #         'SBK_check': False,
    #         'quantity': 1,
    #         'renting_group': 'MBK',
    #     })
    #     self.assertEqual(response.status_code, 200)


class RentalPeriodTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'email': 'example@stud.ntnu.no',
            'first_name': 'Test',
            'last_name': 'Testesen',
            'allergies': '',
            'enrollment_year': 2018,
            'year': 3,
            'is_active': True
            }
        self.user = Mannhullitt.objects.create(**self.credentials)
        self.user.set_password('secret')
        self.user.save()

        self.object_data = {
            'title': "Havfruen",
            'MBK_check': True,
            'SBK_check': False,
            'quantity': 1,
            'renting_group': 'MBK',
        }
        self.rental_object = RentalObject.objects.create(**self.object_data)

        self.period_data = {
            'rental_object': self.rental_object,
            'renter': self.user,
            'approved': False,
            'start_time': timezone.now(),
            'end_time': timezone.now() + timezone.timedelta(days=1),
        }
        self.rental_period = RentalPeriod.objects.create(**self.period_data)

    # def test_delete_success(self):
    #     self.client.login(username='testuser', password='secret')
    #     response = self.client.post(reverse('rental/rentalperiod:delete', args=[self.rental_period.pk]))
    #     self.assertEqual(response.status_code, 302)

    # def test_create_rental_period(self):
    #     self.client.login(username='testuser', password='secret')
    #     response = self.client.post(reverse('rental/rentalperiod:create'), data={
    #         'rental_object': self.rental_object,
    #         'renter': self.user,
    #         'approved': False,
    #         'start_time': timezone.now(),
    #         'end_time': timezone.now() + timezone.timedelta(days=1),
    #     })
    #     self.assertEqual(response.status_code, 200)

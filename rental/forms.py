from django import forms
from django.contrib.admin import widgets
from .models import RentalPeriod


class RentalPeriodForm(forms.ModelForm):
    start_time = forms.SplitDateTimeField(
        required=True,
        widget=widgets.AdminSplitDateTime,
        label='Startdato')
    end_time = forms.SplitDateTimeField(
        required=True,
        widget=widgets.AdminSplitDateTime,
        label='Sluttdato')

    def clean(self):
        cleaned_data = super().clean()
        start_datetime = cleaned_data.get('start_time')
        end_datetime = cleaned_data.get('end_time')
        rental_object = self.instance.rental_object

        # Check if end_time is before start_time
        if start_datetime and end_datetime and end_datetime <= start_datetime:
            raise forms.ValidationError("Sluttid må være etter starttid.")

        if start_datetime and end_datetime:
            # Get the count of objects rented during this period
            rented_objects_count = RentalPeriod.objects.filter(
                rental_object=rental_object,
                start_time__lt=end_datetime,
                end_time__gt=start_datetime
            ).count()

            # Check if the number of objects rented exceeds the quantity
            if rented_objects_count >= rental_object.quantity:
                raise forms.ValidationError("Leie perioden er ikke ledig, sjekk utleiekalender.")

        return cleaned_data

    class Meta:
        model = RentalPeriod
        fields = ['start_time', 'end_time']

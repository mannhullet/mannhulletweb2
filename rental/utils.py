from calendar import HTMLCalendar
from .models import RentalPeriod
from datetime import datetime, timedelta


def from_iso_calendar(year, week, day):
    # Get the fourth day of the given year (this ensures we're in the correct week for any year)
    fourth_of_jan = datetime(year, 1, 4).date()
    # Find the day of the week for the fourth of January (0 = Monday, 1 = Tuesday, etc.)
    delta = fourth_of_jan.weekday()
    # Go back to the start of the week for the fourth of January
    start_of_year = fourth_of_jan - timedelta(days=delta)
    # Compute the desired date
    return start_of_year + timedelta(weeks=week-1, days=day-1)


class RentalCalendarWeek(HTMLCalendar):
    def __init__(self, year=None, week=None):
        self.year = year
        self.week = week
        super(RentalCalendarWeek, self).__init__()

    def formatweek(self, theweek, rentals):
        week = ''
        for day in theweek:
            week += self.formatday(day, rentals)
        return f'<tr> {week} </tr>'

    def formatday(self, day, rentals):
        rentals_for_day = rentals.filter(
            start_time__date__lte=day,  # start time is on or before the day
            end_time__date__gte=day  # end time is on or after the day
        )
        rental_list = '<ul class="rental-list">'
        for rental in rentals_for_day:
            rental_list += f'<li class="rental-item">{rental.rental_object.title}</li>'
        rental_list += '</ul>'

        if day != 0:  # to avoid formatting for days outside the current month
            return f"<td class='calendar-day'><span class='date'>{day.day}</span>{rental_list}</td>"
        return "<td></td>"

    def formatweekheader(self):
        s = ""
        for day in ['Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag', 'Søndag']:
            s += f"<th class='{day.lower()}'>{day}</th>"
        return s

    def formatweekview(self):
        start_of_week = from_iso_calendar(self.year, self.week, 1)
        end_of_week = from_iso_calendar(self.year, self.week, 7)

        rentals = RentalPeriod.objects.filter(
            start_time__date__lte=end_of_week,  # start time is on or before the end of the week
            end_time__date__gte=start_of_week  # end time is on or after the start of the week
        )

        days = [from_iso_calendar(self.year, self.week, i) for i in range(1, 8)]

        week_view = "<table class='calendar'>"
        week_view += f"<thead><tr>{self.formatweekheader()}</tr></thead>"
        week_view += f"<tbody>{self.formatweek(days, rentals)}</tbody>"
        week_view += "</table>"
        return week_view

Django ~= 2.2
Pillow >= 7
django-cleanup >= 4
requests >= 2.24
django-filter >= 2.2.0
django-tinymce >= 2.8.0
django-solo >= 2.0.0
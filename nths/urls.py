from django.urls import path
from . import views


app_name = 'nths'
urlpatterns = [
   path('', views.about_view, name='nths-home'),
   path('program/', views.program_view, name='nths-program'),
   path('reports/', views.reports_view, name='nths-reports'),
   path('statutes/', views.statutes_view, name='nths-statutes'),
   path('partner/', views.sponsors_view, name='nths-partners'),
]

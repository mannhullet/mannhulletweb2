from django.db import models
from django.utils import timezone
from tinymce.models import HTMLField


class nths_member(models.Model):
    first_name = models.CharField(max_length=150, blank=False)
    surname = models.CharField(max_length=150, blank=False)
    mail = models.EmailField(max_length=150, blank=False)
    image = models.ImageField(upload_to='nths_member', null=True, blank=True)
    phonenumber = models.CharField(max_length=150, blank=False)
    nths_leader = models.BooleanField(default=False)

    YEAR_CHOICES = (
        (1, '1. klasse'),
        (2, '2. klasse'),
        (3, '3. klasse'),
        (4, '4. klasse'),
        (5, '5. klasse'),
        (6, '6. klasse'),
    )
    year = models.IntegerField(choices=YEAR_CHOICES, default=1)

    class Meta:
        verbose_name_plural = 'NTHS medlemmer'

    def __str__(self):
        return str(f'{self.first_name} {self.surname}')


class spons(models.Model):
    company = models.CharField(max_length=150, blank=False)
    TIER_CHOICES = (
        (1, 'Ordinær Partner'),
        (2, 'Samarbeidspartner'),
        (3, 'Hovedsamarbeidspartner'),
    )
    tier = models.IntegerField(default=1, choices=TIER_CHOICES)
    logo = models.ImageField(upload_to='spons', null=True, blank=False)
    url = models.CharField(max_length=200, blank=False)

    class Meta:
        verbose_name_plural = 'NTHS sponsorer'

    def __str__(self):
        return self.company


class rapport(models.Model):
    title = models.CharField(max_length=100)
    date = models.DateTimeField(default=timezone.now)
    file = models.FileField()
    country = models.ImageField(upload_to='nths_country',
                                null=True, blank=True)
    description = HTMLField(max_length=375)

    class Meta:
        verbose_name_plural = 'Rapporter'

    def __str__(self):
        return self.title


class program(models.Model):
    year = models.CharField(max_length=150, blank=True)
    program = HTMLField()

    class Meta:
        verbose_name_plural = 'Program'

    def __str__(self):
        return self.year

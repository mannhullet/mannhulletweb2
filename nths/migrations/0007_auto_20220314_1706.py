# Generated by Django 2.2.17 on 2022-03-14 16:06

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('nths', '0006_rapport_country'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rapport',
            name='description',
            field=tinymce.models.HTMLField(max_length=375),
        ),
    ]

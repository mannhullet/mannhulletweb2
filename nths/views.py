from django.shortcuts import render
from .models import nths_member, spons, program, rapport
# Create your views here.


def about_view(request):
    # Order by 'nths_leader' descending (True values first), then by 'year' descending
    nthsmember_list = nths_member.objects.all().order_by('-nths_leader', '-year')
    return render(request, 'nths/about.html', {'nthsmember_list': nthsmember_list})


def program_view(request):
    programs = program.objects.all()
    return render(request, 'nths/program.html', {'programs': programs})


def reports_view(request):
    rapport_list = rapport.objects.all().order_by('-date')
    return render(request, 'nths/reports.html', {'rapport_list': rapport_list})


def sponsors_view(request):
    sponsors_hsp = spons.objects.filter(tier='3')
    sponsors_sp = spons.objects.filter(tier='2')
    sponsors_op = spons.objects.filter(tier='1')
    return render(request, 'nths/sponsors.html',
                  {'sponsors_hsp': sponsors_hsp, 'sponsors_sp': sponsors_sp, 'sponsors_op': sponsors_op},)


def statutes_view(request):
    return render(request, 'nths/statutes.html')

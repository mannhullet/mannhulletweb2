from django.contrib import admin
from .models import nths_member, spons, rapport, program


class NTHSAdmin(admin.ModelAdmin):
    list_display = [
        'nths_member',
        'spons',
        'rapport',
        'program',
    ]


# Register your models here.
admin.site.register(nths_member)
admin.site.register(spons)
admin.site.register(rapport)
admin.site.register(program)
